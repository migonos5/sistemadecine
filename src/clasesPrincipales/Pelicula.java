package clasesPrincipales;

/**
 * *********************************************************************
 * Module: Pelicula.java Author: Usuario Purpose: Defines the Class Pelicula
 * *********************************************************************
 */
import clasesAuxiliares.GeneradorPDF;
import clasesSecundarias.Fecha;
import clasesSecundarias.Hora;
import java.io.Serializable;
import javax.swing.table.DefaultTableModel;

/**
 * @pdOid b41baad8-615f-4994-8e6d-72daba9e0f5a
 */
public class Pelicula implements Serializable {

    /**
     * @pdOid a17e5c77-abfa-411e-9ecb-9a5460607c5a
     */
    private String nombre;
    /**
     * @pdOid 31d27f69-5e27-423a-8f4f-e84a1cccde7d
     */
    private String sinopsis;
    /**
     * @pdOid 59d112b6-9dec-4abf-9d50-dc4d4f68ad4d
     */
    private Hora duracion;
    /**
     * @pdOid 71348d60-d9d3-4859-9899-a3ed99b7a480
     */
    private String clasificacion;
    /**
     * @pdOid b90e65e9-3738-41b7-8b51-c4686b1337ee
     */
    private String categoriaDePelicula;
    /**
     * @pdOid 94ba63a3-e372-4c8b-baa6-ea6b667656a7
     */
    private String dimension;
    /**
     * @pdOid c77e99d6-958c-40e0-bd5f-dbaa58c4b2ab
     */

    private int idDePelicula;
    /**
     * @pdOid 6d2251f8-95e2-4de3-b880-160a89874fbf
     */
    private String rutaDePortada;

    /**
     * @return * @pdOid 038ba306-f82d-47d7-b61b-37040715f553
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param newNombre
     * @pdOid 422022cd-8fdb-40e7-84d8-71b81c025915
     */
    public void setNombre(String newNombre) {
        nombre = newNombre;
    }

    /**
     * @return * @pdOid 75379d65-93ca-49db-a136-86acee12d310
     */
    public String getClasificacion() {
        return clasificacion;
    }

    /**
     * @param newClasificacion
     * @pdOid 1724f4b4-ef1d-4c59-bc88-ba4710a2e372
     */
    public void setClasificacion(String newClasificacion) {
        clasificacion = newClasificacion;
    }

    /**
     * @return * @pdOid c3f5f138-81cc-45c2-9003-95b0a3af7b81
     */
    public String getCategoriaDePelicula() {
        return categoriaDePelicula;
    }

    /**
     * @param newCategoriaDePelicula
     * @pdOid c1bdc152-c0b8-4b31-84e1-9c81bd3c630b
     */
    public void setCategoriaDePelicula(String newCategoriaDePelicula) {
        categoriaDePelicula = newCategoriaDePelicula;
    }

    /**
     * @return * @pdOid 4089de04-42d2-4b2c-bf91-0beac356ed6b
     */
    public String getDimension() {
        return dimension;
    }

    /**
     * @param newDimension
     * @pdOid c1e48980-be35-41ad-bc38-5f953f51e1fb
     */
    public void setDimension(String newDimension) {
        dimension = newDimension;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public Hora getDuracion() {
        return duracion;
    }

    public void setDuracion(Hora duracion) {
        this.duracion = duracion;
    }

    public int getIdDePelicula() {
        return idDePelicula;
    }

    public void setIdDePelicula(int idDePelicula) {
        this.idDePelicula = idDePelicula;
    }

    public String getRutaDePortada() {
        return rutaDePortada;
    }

    public void setRutaDePortada(String rutaDePortada) {
        this.rutaDePortada = rutaDePortada;
    }

    // Constructor
    public Pelicula(String nombre, String sinopsis, Hora duracion, String clasificacion, String categoriaDePelicula, String dimension, int idDePelicula, String rutaDePortada) {
        this.nombre = nombre;
        this.sinopsis = sinopsis;
        this.duracion = duracion;
        this.clasificacion = clasificacion;
        this.categoriaDePelicula = categoriaDePelicula;
        this.dimension = dimension;
        this.idDePelicula = idDePelicula;
        this.rutaDePortada = rutaDePortada;
    }

    public void representarEnTabla(DefaultTableModel tabla) {
        tabla.addRow(new Object[]{
            this.nombre,
            this.sinopsis,
            this.duracion,
            this.dimension,
            this.categoriaDePelicula,
            this.clasificacion
        });
    }

    public void representarEnPDF(String ruta) {
        new GeneradorPDF().generarPDF(
                "Comprobante de registro de película",
                "Id. de registro / Id. de pelicula: " + this.getIdDePelicula(),
                "Nombre / Titulo: " + this.nombre + "\n"
                + "Sinópsis: " + this.sinopsis + "\n"
                + "Duración: " + this.duracion.toString() + "\n"
                + "Dimensión: " + this.dimension + "\n"
                + "Categoría: " + this.categoriaDePelicula + "\n"
                + "Clasificación: " + this.clasificacion,
                "Comprobante generado el: " + new Fecha().representar(),
                getClass().getResource("/medios/imagenRegistroPelicula.jpg").toString(),
                ruta + ".pdf"
        );
    }
}
