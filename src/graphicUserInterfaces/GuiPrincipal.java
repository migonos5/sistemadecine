/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicUserInterfaces;

import clasesAuxiliares.ValidadorDeCampos;
import clasesAuxiliares.img;
import clasesPrincipales.Cine;
import clasesPrincipales.Factura;
import clasesPrincipales.Funcion;
import clasesPrincipales.Pelicula;
import clasesSecundarias.Hora;
import exceptions.exceptionHoraIncorrecta;
import exceptions.exceptionMinutosIncorrectos;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Usuario
 */
public class GuiPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form GuiPrincipa
     */
    // Atributos
    private Cine cinema;
    private DefaultTableModel tabla1;
    private DefaultTableModel tabla2;
    private DefaultTableModel tabla3;
    private DefaultTableModel tabla4;
    private DefaultTableModel tabla5;
    private DefaultTableModel tabla6;
    private DefaultTableModel tabla7;
    private DefaultTableModel tabla8;
    private DefaultTableModel tabla9;
    private JToggleButton[][] toggleBtnAsientos;

    // Atributos auxiliares
    private HashMap<Integer, Pelicula> auxFiltroDePeliculasPorCategoria;
    private HashMap<Integer, Pelicula> auxFiltroDePeliculasPorDimension;
    private Pelicula auxPeliculaSeleccionada;
    private HashMap<Integer, Funcion> auxFiltroDeFuncionesPorCategoria;
    private HashMap<Integer, Funcion> auxFiltroDeFuncionesPorDia;
    private HashMap<Integer, Funcion> auxFiltroDeFuncionesPorDimension;
    private HashMap<Integer, Funcion> auxFiltroDeFuncionesPorSeccionDelDia;
    private HashMap<Integer, Funcion> auxFiltroDeFuncionesPorHorario;
    private Funcion auxFuncionSeleccionada;
    private int auxNumeroDeBoletosDeseados;

    public GuiPrincipal() {
        // Especificaciones de apertura del formulario
        initComponents();
        this.setSize(1000, 500);
        this.setLocationRelativeTo(null);

        // Recuperación de datos
        try {
            ObjectInputStream auxAgencia;
            auxAgencia = new ObjectInputStream(new FileInputStream("archivo.dat"));
            this.cinema = (Cine) auxAgencia.readObject();
            auxAgencia.close();
        } catch (Exception e) {
            System.out.println("" + e);
            this.cinema = new Cine("Cine", "Star Cines");
        }

        // Inicializacion de atributos
        this.tabla1 = (DefaultTableModel) tabla1RegistrosDePeliculasRecientesRP.getModel();
        this.tabla2 = (DefaultTableModel) tabla2Cartelera1CF.getModel();
        this.tabla3 = (DefaultTableModel) tabla3Cartelera2CF.getModel();
        this.tabla4 = (DefaultTableModel) tabla4DetallesDeLaPeliculaDCF.getModel();
        this.tabla5 = (DefaultTableModel) tabla5FuncionesRegistradasRecientementeDCF.getModel();
        this.tabla6 = (DefaultTableModel) tabla6PeliculasFiltradasCDB.getModel();
        this.tabla7 = (DefaultTableModel) tabla7PeliculasFiltradasCDB.getModel();
        this.tabla8 = (DefaultTableModel) tabla8CostoDeLosBoletosDF.getModel();
        this.tabla9 = (DefaultTableModel) tabla9CostoDeLosBoletosNBC.getModel();

        // Inicializacion de atributos auxiliares
        this.auxFiltroDePeliculasPorCategoria = new HashMap<>();
        this.auxFiltroDePeliculasPorDimension = new HashMap<>();
        this.auxFiltroDeFuncionesPorCategoria = new HashMap<>();
        this.auxFiltroDeFuncionesPorDia = new HashMap<>();
        this.auxFiltroDeFuncionesPorDimension = new HashMap<>();
        this.auxFiltroDeFuncionesPorSeccionDelDia = new HashMap<>();
        this.auxFiltroDeFuncionesPorHorario = new HashMap<>();

        //
        txfClasificacionDePeliculaRP.setEditable(false);
        txfRutaDeLaPortadaRP.setEditable(false);

        // Validaciones de campos
        ValidadorDeCampos.validarSoloLetras(txfSinopsisDeLaPeliculaRP);
        ValidadorDeCampos.validarSoloNumeros(txfCostoDeLaEntradaDCF);
        ValidadorDeCampos.limitarCaracteres(txfCostoDeLaEntradaDCF, 2);
        ValidadorDeCampos.validarSoloNumeros(txfNumeroDeBoletosDeNiñoNBC);
        ValidadorDeCampos.validarSoloNumeros(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC);
        ValidadorDeCampos.validarSoloNumeros(txfNumeroDeBoletosDeAdultoNBC);
        ValidadorDeCampos.validarSoloNumeros(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC);
        ValidadorDeCampos.validarSoloNumeros(txfNumeroDeBoletosDeAdultoMayorNBC);
        ValidadorDeCampos.validarSoloNumeros(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC);
        ValidadorDeCampos.validarSoloLetras(txfNombreDelClienteF);
        ValidadorDeCampos.validarSoloNumeros(txfCedulaDelClienteF);
        ValidadorDeCampos.validarSoloNumeros(txfTelefonoDelClienteF);
        ValidadorDeCampos.validarSoloLetras(txfDireccionDelClienteF);
        ValidadorDeCampos.validarSoloNumeros(txfNumeroDeTarjetaF);
        ValidadorDeCampos.limitarCaracteres(txfNumeroDeTarjetaF, 10);
        ValidadorDeCampos.limitarCaracteres(txfCedulaDelClienteF, 10);
        ValidadorDeCampos.limitarCaracteres(txfTelefonoDelClienteF, 10);

        // Inicializacion de otros
        crearEstructuraDeAsientos();
    }

    public class AccionBotones implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 10; j++) {
                    if (ae.getSource().equals(toggleBtnAsientos[i][j])) {
                        if (auxNumeroDeBoletosDeseados >= 1) {
                            if (toggleBtnAsientos[i][j].isSelected()) {
                                toggleBtnAsientos[i][j].setBackground(Color.BLUE);
                                auxFuncionSeleccionada.getSalaDondeSeProyectara()[i][j] = true;
                                auxNumeroDeBoletosDeseados--;
                                System.out.println("" + auxNumeroDeBoletosDeseados);
                            } else {
                                toggleBtnAsientos[i][j].setBackground(Color.GREEN);
                                auxFuncionSeleccionada.getSalaDondeSeProyectara()[i][j] = false;
                                auxNumeroDeBoletosDeseados++;
                                System.out.println("" + auxNumeroDeBoletosDeseados);
                            }
                        } else {
                            if (toggleBtnAsientos[i][j].isSelected()) {
                                toggleBtnAsientos[i][j].setSelected(false);
                            } else if (!toggleBtnAsientos[i][j].isSelected() && auxFuncionSeleccionada.getSalaDondeSeProyectara()[i][j]) {
                                toggleBtnAsientos[i][j].setBackground(Color.GREEN);
                                auxFuncionSeleccionada.getSalaDondeSeProyectara()[i][j] = false;
                                auxNumeroDeBoletosDeseados++;
                                System.out.println("" + auxNumeroDeBoletosDeseados);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        GuiMenuPrincipal = new javax.swing.JFrame();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnOpenVistaDelAdministrador = new javax.swing.JButton();
        btnOpenVistaDelCliente = new javax.swing.JButton();
        jLabel69 = new javax.swing.JLabel();
        background2 = new javax.swing.JLabel();
        GuiMenuPrincipalAdministrador = new javax.swing.JFrame();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        btnOpenCrearFuncion = new javax.swing.JButton();
        btnOpenRegistrarPelicula = new javax.swing.JButton();
        jLabel71 = new javax.swing.JLabel();
        background3 = new javax.swing.JLabel();
        GuiRegistroDePeliculas = new javax.swing.JFrame();
        jPanel5 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txfRutaDeLaPortadaRP = new javax.swing.JTextField();
        btnSeleccionarPortadaRP = new javax.swing.JButton();
        txfNombreDeLaPeliculaRP = new javax.swing.JTextField();
        txfSinopsisDeLaPeliculaRP = new javax.swing.JTextField();
        comboCategoriaDePeliculaRP = new javax.swing.JComboBox<>();
        comboDimensionDeLaPeliculaRP = new javax.swing.JComboBox<>();
        comboClasificacionDeLaPeliculaRP = new javax.swing.JComboBox<>();
        btnRegistrarPeliculaRP = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla1RegistrosDePeliculasRecientesRP = new javax.swing.JTable();
        spinnerDuracionDeLaPeliculaRP = new javax.swing.JSpinner();
        txfClasificacionDePeliculaRP = new javax.swing.JTextField();
        jLabel73 = new javax.swing.JLabel();
        background5 = new javax.swing.JLabel();
        GuiCreacionDeFunciones = new javax.swing.JFrame();
        jPanel6 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        comboFitroPorCategoriaCF = new javax.swing.JComboBox<>();
        jLabel31 = new javax.swing.JLabel();
        comboFiltrarPeliculaPorDimensionCF = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabla2Cartelera1CF = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabla3Cartelera2CF = new javax.swing.JTable();
        jLabel75 = new javax.swing.JLabel();
        background6 = new javax.swing.JLabel();
        GuiDetallesDeLaCreacionDeFunciones = new javax.swing.JFrame();
        jPanel7 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabla5FuncionesRegistradasRecientementeDCF = new javax.swing.JTable();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        comboDiaDeLaFuncionDCF = new javax.swing.JComboBox<>();
        spinnerHoraDeInicioDeLaFuncionDCF = new javax.swing.JSpinner();
        spinnerPorcentajeBoletoAdultoDCF = new javax.swing.JSpinner();
        spinnerPorcentajeBoletoNiñoDCF = new javax.swing.JSpinner();
        spinnerPorcentajeBoletoAdultoMayorDCF = new javax.swing.JSpinner();
        spinnerPorcentajeBoletoDiscapacitadoDCF = new javax.swing.JSpinner();
        txfCostoDeLaEntradaDCF = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tabla4DetallesDeLaPeliculaDCF = new javax.swing.JTable();
        btnCrearFuncionDCF = new javax.swing.JButton();
        jLabel76 = new javax.swing.JLabel();
        background7 = new javax.swing.JLabel();
        GuiMenuPrincipalDelCliente = new javax.swing.JFrame();
        jPanel8 = new javax.swing.JPanel();
        jLabel49 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel77 = new javax.swing.JLabel();
        background8 = new javax.swing.JLabel();
        GuiCompraDeBoletosSeleccionDeFuncion = new javax.swing.JFrame();
        jPanel9 = new javax.swing.JPanel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        comboFiltroPorDimensionCDB = new javax.swing.JComboBox<>();
        comboFitroPorCategoriaCDB = new javax.swing.JComboBox<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        tabla6PeliculasFiltradasCDB = new javax.swing.JTable();
        jScrollPane7 = new javax.swing.JScrollPane();
        tabla7PeliculasFiltradasCDB = new javax.swing.JTable();
        jLabel61 = new javax.swing.JLabel();
        comboFiltroPorDiaCDB = new javax.swing.JComboBox<>();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        comboFiltroPorSeccionDelDiaCDB = new javax.swing.JComboBox<>();
        jLabel56 = new javax.swing.JLabel();
        comboFiltroPorHoraCDB = new javax.swing.JComboBox<>();
        jLabel78 = new javax.swing.JLabel();
        background9 = new javax.swing.JLabel();
        GuiDetallesDeLaFuncion = new javax.swing.JFrame();
        jPanel10 = new javax.swing.JPanel();
        jLabel57 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        lblNombreDeLaPeliculaDF = new javax.swing.JLabel();
        lblPortadaDeLaPeliculaDF = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        lblSinopsisDeLaPeliculaDF = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        lblDuracionDeLaPeliculaDF = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        lblCategoriaDeLaPeliculaDF = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        lblClasificacionDeLaPeliculaDF = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tabla8CostoDeLosBoletosDF = new javax.swing.JTable();
        btnBuscarOtrasFuncionesDF = new javax.swing.JButton();
        btnComprarBoletosDF = new javax.swing.JButton();
        jLabel79 = new javax.swing.JLabel();
        background10 = new javax.swing.JLabel();
        GuiNumeroDeBoletosAComprar = new javax.swing.JFrame();
        jPanel11 = new javax.swing.JPanel();
        jLabel80 = new javax.swing.JLabel();
        jLabel81 = new javax.swing.JLabel();
        jLabel82 = new javax.swing.JLabel();
        jLabel83 = new javax.swing.JLabel();
        btnSeleccionarAsientosNBC = new javax.swing.JButton();
        jLabel85 = new javax.swing.JLabel();
        txfNumeroDeBoletosDeAdultoConDiscapacidadNBC = new javax.swing.JTextField();
        jLabel86 = new javax.swing.JLabel();
        txfNumeroDeBoletosDeAdolescenteNBC = new javax.swing.JTextField();
        txfNumeroDeBoletosDeAdultoMayorNBC = new javax.swing.JTextField();
        jLabel87 = new javax.swing.JLabel();
        jLabel88 = new javax.swing.JLabel();
        txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC = new javax.swing.JTextField();
        jLabel89 = new javax.swing.JLabel();
        txfNumeroDeBoletosDeNiñoNBC = new javax.swing.JTextField();
        jLabel90 = new javax.swing.JLabel();
        txfNumeroDeBoletosDeNiñoConDiscapacidadNBC = new javax.swing.JTextField();
        jLabel96 = new javax.swing.JLabel();
        txfNumeroDeBoletosDeAdultoNBC = new javax.swing.JTextField();
        txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC = new javax.swing.JTextField();
        jLabel97 = new javax.swing.JLabel();
        jLabel98 = new javax.swing.JLabel();
        jLabel94 = new javax.swing.JLabel();
        jLabel91 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tabla9CostoDeLosBoletosNBC = new javax.swing.JTable();
        jLabel92 = new javax.swing.JLabel();
        lblBoletosDisponiblesNBC = new javax.swing.JLabel();
        jLabel95 = new javax.swing.JLabel();
        lblNumeroDeBoletosCompradosNBC = new javax.swing.JLabel();
        jLabel100 = new javax.swing.JLabel();
        lblValorDeLaCompraActualNBC = new javax.swing.JLabel();
        jLabel102 = new javax.swing.JLabel();
        background11 = new javax.swing.JLabel();
        GuiSeleccionDeAsientos = new javax.swing.JFrame();
        jPanel12 = new javax.swing.JPanel();
        jLabel84 = new javax.swing.JLabel();
        jLabel93 = new javax.swing.JLabel();
        jLabel99 = new javax.swing.JLabel();
        jLabel101 = new javax.swing.JLabel();
        panelAsientosDeCineSA = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel103 = new javax.swing.JLabel();
        jLabel104 = new javax.swing.JLabel();
        jLabel105 = new javax.swing.JLabel();
        jLabel106 = new javax.swing.JLabel();
        jLabel107 = new javax.swing.JLabel();
        jLabel109 = new javax.swing.JLabel();
        jLabel110 = new javax.swing.JLabel();
        jLabel112 = new javax.swing.JLabel();
        btnFacturarYPagarSA = new javax.swing.JButton();
        background12 = new javax.swing.JLabel();
        GuiFacturacion = new javax.swing.JFrame();
        jPanel14 = new javax.swing.JPanel();
        jLabel108 = new javax.swing.JLabel();
        jLabel111 = new javax.swing.JLabel();
        jLabel113 = new javax.swing.JLabel();
        jLabel114 = new javax.swing.JLabel();
        jLabel115 = new javax.swing.JLabel();
        jLabel116 = new javax.swing.JLabel();
        txfNombreDelClienteF = new javax.swing.JTextField();
        jLabel117 = new javax.swing.JLabel();
        txfCedulaDelClienteF = new javax.swing.JTextField();
        jLabel118 = new javax.swing.JLabel();
        txfTelefonoDelClienteF = new javax.swing.JTextField();
        jLabel119 = new javax.swing.JLabel();
        txfDireccionDelClienteF = new javax.swing.JTextField();
        jLabel120 = new javax.swing.JLabel();
        btnFacturarF = new javax.swing.JButton();
        jLabel121 = new javax.swing.JLabel();
        lblSubtotalF = new javax.swing.JLabel();
        jLabel123 = new javax.swing.JLabel();
        jLabel124 = new javax.swing.JLabel();
        lblIva12F = new javax.swing.JLabel();
        lblTotalF = new javax.swing.JLabel();
        jLabel122 = new javax.swing.JLabel();
        txfNumeroDeTarjetaF = new javax.swing.JTextField();
        jLabel125 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        background13 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnOpenMenuPrincipal = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        background1 = new javax.swing.JLabel();

        GuiMenuPrincipal.setUndecorated(true);
        GuiMenuPrincipal.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel2.setText("Menú principal");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 10, -1, -1));

        btnOpenVistaDelAdministrador.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btnOpenVistaDelAdministrador.setForeground(new java.awt.Color(255, 0, 0));
        btnOpenVistaDelAdministrador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/portadasDePeliculas/user_group_man_woman_50px.png"))); // NOI18N
        btnOpenVistaDelAdministrador.setText("Vista del administrador");
        btnOpenVistaDelAdministrador.setContentAreaFilled(false);
        btnOpenVistaDelAdministrador.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnOpenVistaDelAdministrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenVistaDelAdministradorActionPerformed(evt);
            }
        });
        jPanel2.add(btnOpenVistaDelAdministrador, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 230, -1, -1));

        btnOpenVistaDelCliente.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btnOpenVistaDelCliente.setForeground(new java.awt.Color(255, 0, 0));
        btnOpenVistaDelCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/portadasDePeliculas/user_50px.png"))); // NOI18N
        btnOpenVistaDelCliente.setText("Vista del cliente");
        btnOpenVistaDelCliente.setContentAreaFilled(false);
        btnOpenVistaDelCliente.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnOpenVistaDelCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenVistaDelClienteActionPerformed(evt);
            }
        });
        jPanel2.add(btnOpenVistaDelCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 240, -1, -1));

        jLabel69.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
        jLabel69.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel69MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel69MousePressed(evt);
            }
        });
        jPanel2.add(jLabel69, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        background2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
        jPanel2.add(background2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        GuiMenuPrincipal.getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

        GuiMenuPrincipalAdministrador.setUndecorated(true);
        GuiMenuPrincipalAdministrador.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel7MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel8MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel9.setText("Administrador");
        jPanel3.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, -1, -1));

        btnOpenCrearFuncion.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btnOpenCrearFuncion.setForeground(new java.awt.Color(255, 0, 0));
        btnOpenCrearFuncion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/portadasDePeliculas/movie_50px.png"))); // NOI18N
        btnOpenCrearFuncion.setText("Crear función");
        btnOpenCrearFuncion.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0)));
        btnOpenCrearFuncion.setContentAreaFilled(false);
        btnOpenCrearFuncion.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnOpenCrearFuncion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenCrearFuncionActionPerformed(evt);
            }
        });
        jPanel3.add(btnOpenCrearFuncion, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 230, 150, -1));

        btnOpenRegistrarPelicula.setFont(new java.awt.Font("Century", 1, 14)); // NOI18N
        btnOpenRegistrarPelicula.setForeground(new java.awt.Color(255, 0, 0));
        btnOpenRegistrarPelicula.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/portadasDePeliculas/movie_projector_50px.png"))); // NOI18N
        btnOpenRegistrarPelicula.setText("Registrar película");
        btnOpenRegistrarPelicula.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0)));
        btnOpenRegistrarPelicula.setContentAreaFilled(false);
        btnOpenRegistrarPelicula.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnOpenRegistrarPelicula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenRegistrarPeliculaActionPerformed(evt);
            }
        });
        jPanel3.add(btnOpenRegistrarPelicula, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 220, -1, -1));

        jLabel71.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
        jLabel71.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel71MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel71, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        background3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
        jPanel3.add(background3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        GuiMenuPrincipalAdministrador.getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

        GuiRegistroDePeliculas.setUndecorated(true);
        GuiRegistroDePeliculas.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
        jLabel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel13MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
        jLabel14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel14MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel15.setText("Registro de películas");
        jPanel5.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, -1, -1));

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel16.setText("Registros recientes:");
        jPanel5.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 360, -1, -1));

        jLabel17.setText("Nombre:");
        jPanel5.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        jLabel18.setText("Sinópsis:");
        jPanel5.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));

        jLabel19.setText("Duración:");
        jPanel5.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, -1, -1));

        jLabel20.setText("Clasificación:");
        jPanel5.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, -1, -1));

        jLabel21.setText("Categoría:");
        jPanel5.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, -1, -1));

        jLabel22.setText("Dimensión:");
        jPanel5.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, -1, -1));

        jLabel23.setText("Ruta de la portada:");
        jPanel5.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, -1, -1));

        txfRutaDeLaPortadaRP.setText("C:\\imgaux\\portadasDePeliculas\\");
            jPanel5.add(txfRutaDeLaPortadaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 300, 750, -1));

            btnSeleccionarPortadaRP.setText("Seleccionar imagen");
            btnSeleccionarPortadaRP.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnSeleccionarPortadaRPActionPerformed(evt);
                }
            });
            jPanel5.add(btnSeleccionarPortadaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 300, 100, -1));
            jPanel5.add(txfNombreDeLaPeliculaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 860, -1));

            txfSinopsisDeLaPeliculaRP.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfSinopsisDeLaPeliculaRPActionPerformed(evt);
                }
            });
            jPanel5.add(txfSinopsisDeLaPeliculaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 150, 860, -1));

            comboCategoriaDePeliculaRP.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Acción", "Aventura", "Terror", "Comedia", "Familiar", "Infantil", "Drama", "Ciencia ficción", "Suspenso", "Romance" }));
            comboCategoriaDePeliculaRP.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboCategoriaDePeliculaRPActionPerformed(evt);
                }
            });
            jPanel5.add(comboCategoriaDePeliculaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 240, 860, -1));

            comboDimensionDeLaPeliculaRP.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2D", "3D", "4D" }));
            jPanel5.add(comboDimensionDeLaPeliculaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 210, 860, -1));

            comboClasificacionDeLaPeliculaRP.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "B", "B15", "C", "D" }));
            comboClasificacionDeLaPeliculaRP.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboClasificacionDeLaPeliculaRPActionPerformed(evt);
                }
            });
            jPanel5.add(comboClasificacionDeLaPeliculaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 270, 100, -1));

            btnRegistrarPeliculaRP.setText("Registrar película");
            btnRegistrarPeliculaRP.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnRegistrarPeliculaRPActionPerformed(evt);
                }
            });
            jPanel5.add(btnRegistrarPeliculaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 970, -1));

            jLabel24.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel24.setText("Detalles de la película:");
            jPanel5.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

            tabla1RegistrosDePeliculasRecientesRP.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {
                    "Nombre / Titulo", "Sinópsis", "Duración", "Dimensión", "Categoría", "Clasificación"
                }
            ));
            jScrollPane1.setViewportView(tabla1RegistrosDePeliculasRecientesRP);

            jPanel5.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 390, 970, 100));

            spinnerDuracionDeLaPeliculaRP.setModel(new javax.swing.SpinnerListModel(new String[] {"00:45", "00:46", "00:47", "00:48", "00:49", "00:50", "00:51", "00:52", "00:53", "00:54", "00:56", "00:57", "00:58", "00:59", "01:00", "01:01", "01:02", "01:03", "01:04", "01:05", "01:06", "01:07", "01:08", "01:09", "01:10", "01:11", "01:12", "01:13", "01:14", "01:15", "01:16", "01:17", "01:18", "01:19", "01:20", "01:21", "01:22", "01:23", "01:24", "01:25", "01:26", "01:27", "01:28", "01:29", "01:30", "01:31", "01:32", "01:33", "01:34", "01:35", "01:36", "01:37", "01:38", "01:39", "01:40", "01:41", "01:42", "01:43", "01:44", "01:45", "01:46", "01:47", "01:48", "01:49", "01:50", "01:51", "01:52", "01:53", "01:54", "01:56", "01:57", "01:58", "01:59", "02:00", "02:01", "02:02", "02:03", "02:04", "02:05", "02:06", "02:07", "02:08", "02:09", "02:10", "02:11", "02:12", "02:13", "02:14", "02:15", "02:16", "02:17", "02:18", "02:19", "02:20", "02:21", "02:22", "02:23", "02:24", "02:25", "02:26", "02:27", "02:28", "02:29", "02:30", "02:31", "02:32", "02:33", "02:34", "02:35", "02:36", "02:37", "02:38", "02:39", "02:40", "02:41", "02:42", "02:43", "02:44", "02:45", "02:46", "02:47", "02:48", "02:49", "02:50", "02:51", "02:52", "02:53", "02:54", "02:56", "02:57", "02:58", "02:59", "00:00", "03:01", "03:02", "03:03", "03:04", "03:05", "03:06", "03:07", "03:08", "03:09", "03:10", "03:11", "03:12", "03:13", "03:14", "03:15", "03:16", "03:17", "03:18", "03:19", "03:20", "03:21", "03:22", "03:23", "03:24", "03:25", "03:26", "03:27", "03:28", "03:29", "03:30", "03:31", "03:32", "03:33", "03:34", "03:35", "03:36", "03:37", "03:38", "03:39", "03:40", "03:41", "03:42", "03:43", "03:44", "03:45", "03:46", "03:47", "03:48", "03:49", "03:50", "03:51", "03:52", "03:53", "03:54", "03:56", "03:57", "03:58", "03:59", "04:00"}));
            jPanel5.add(spinnerDuracionDeLaPeliculaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 180, 860, -1));

            txfClasificacionDePeliculaRP.setText("Películas para adolescentes de 12 años en adelante.");
            txfClasificacionDePeliculaRP.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfClasificacionDePeliculaRPActionPerformed(evt);
                }
            });
            jPanel5.add(txfClasificacionDePeliculaRP, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 270, 750, -1));

            jLabel73.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
            jLabel73.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel73MouseClicked(evt);
                }
            });
            jPanel5.add(jLabel73, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

            background5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel5.add(background5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            GuiRegistroDePeliculas.getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            GuiCreacionDeFunciones.setUndecorated(true);
            GuiCreacionDeFunciones.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
            jLabel25.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel25MouseClicked(evt);
                }
            });
            jPanel6.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

            jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
            jLabel26.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel26MouseClicked(evt);
                }
            });
            jPanel6.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

            jLabel27.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
            jLabel27.setText("Creación de funciones");
            jPanel6.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 10, -1, -1));

            jLabel28.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel28.setText("Selección de película");
            jPanel6.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, -1, -1));

            jLabel29.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
            jLabel29.setText("Filtrar por:");
            jPanel6.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, -1));

            jLabel30.setText("Categoría:");
            jPanel6.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, -1, -1));

            comboFitroPorCategoriaCF.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione una categoría", "Acción", "Aventura", "Terror", "Comedia", "Familiar", "Infantil", "Drama", "Ciencia ficción", "Suspenso", "Romance" }));
            comboFitroPorCategoriaCF.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboFitroPorCategoriaCFActionPerformed(evt);
                }
            });
            jPanel6.add(comboFitroPorCategoriaCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 110, -1, -1));

            jLabel31.setText("Dimensión:");
            jPanel6.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 110, -1, -1));

            comboFiltrarPeliculaPorDimensionCF.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2D", "3D", "4D" }));
            comboFiltrarPeliculaPorDimensionCF.setEnabled(false);
            comboFiltrarPeliculaPorDimensionCF.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboFiltrarPeliculaPorDimensionCFActionPerformed(evt);
                }
            });
            jPanel6.add(comboFiltrarPeliculaPorDimensionCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 110, -1, -1));

            tabla2Cartelera1CF.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {

                }
            ));
            tabla2Cartelera1CF.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tabla2Cartelera1CFMouseClicked(evt);
                }
            });
            jScrollPane2.setViewportView(tabla2Cartelera1CF);

            jPanel6.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, -1, 350));

            tabla3Cartelera2CF.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {

                }
            ));
            tabla3Cartelera2CF.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tabla3Cartelera2CFMouseClicked(evt);
                }
            });
            jScrollPane3.setViewportView(tabla3Cartelera2CF);

            jPanel6.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 140, -1, 350));

            jLabel75.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
            jLabel75.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel75MouseClicked(evt);
                }
            });
            jPanel6.add(jLabel75, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

            background6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel6.add(background6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            GuiCreacionDeFunciones.getContentPane().add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            GuiDetallesDeLaCreacionDeFunciones.setUndecorated(true);
            GuiDetallesDeLaCreacionDeFunciones.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
            jLabel32.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel32MouseClicked(evt);
                }
            });
            jPanel7.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

            jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
            jLabel33.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel33MouseClicked(evt);
                }
            });
            jPanel7.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

            jLabel34.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
            jLabel34.setText("Detalles de la creación de función");
            jPanel7.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, -1, -1));

            tabla5FuncionesRegistradasRecientementeDCF.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {
                    "Película", "Día", "Hora", "Sección del día", "C. B. Adulto", "C. B. Adulto discapacitado ", "C. B. Adulto mayor", "C. B. Adulto mayor discapacitado", "C. B. Niño", "C. B. Niño discapacitado"
                }
            ));
            jScrollPane4.setViewportView(tabla5FuncionesRegistradasRecientementeDCF);

            jPanel7.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 430, 970, 60));

            jLabel35.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel35.setText("Detalles de la película:");
            jPanel7.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

            jLabel36.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel36.setText("Funciones registradas recientemente");
            jPanel7.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 400, -1, -1));

            jLabel37.setText("Día:");
            jPanel7.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 190, -1, -1));

            jLabel38.setText("Hora de inicio:");
            jPanel7.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 220, -1, -1));

            jLabel39.setText("Costo de la entrada general:");
            jPanel7.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 250, -1, -1));

            jLabel40.setText("Porcentaje de descuento para el boleto de adulto:");
            jPanel7.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 280, -1, -1));

            jLabel41.setText("Porcentaje de descuento para el boleto de niño:");
            jPanel7.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 310, -1, -1));

            jLabel42.setText("Porcentaje de descuento para el boleto de adultos mayores:");
            jPanel7.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 340, -1, -1));

            jLabel43.setText("Porcentaje de descuento para el boleto de personas discapacitadas:");
            jPanel7.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 370, -1, -1));

            comboDiaDeLaFuncionDCF.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo" }));
            jPanel7.add(comboDiaDeLaFuncionDCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 190, 400, -1));

            spinnerHoraDeInicioDeLaFuncionDCF.setModel(new javax.swing.SpinnerListModel(new String[] {"08:00", "08:01", "08:02", "08:03", "08:04", "08:05", "08:06", "08:07", "08:08", "08:09", "08:10", "08:11", "08:12", "08:13", "08:14", "08:15", "08:16", "08:17", "08:18", "08:19", "08:20", "08:21", "08:22", "08:23", "08:24", "08:25", "08:26", "08:27", "08:28", "08:29", "08:30", "08:31", "08:32", "08:33", "08:34", "08:35", "08:36", "08:37", "08:38", "08:39", "08:40", "08:41", "08:42", "08:43", "08:44", "08:45", "08:46", "08:47", "08:48", "08:49", "08:50", "08:51", "08:52", "08:53", "08:54", "08:56", "08:57", "08:58", "08:59", "09:00", "09:01", "09:02", "09:03", "09:04", "09:05", "09:06", "09:07", "09:08", "09:09", "09:10", "09:11", "09:12", "09:13", "09:14", "09:15", "09:16", "09:17", "09:18", "09:19", "09:20", "09:21", "09:22", "09:23", "09:24", "09:25", "09:26", "09:27", "09:28", "09:29", "09:30", "09:31", "09:32", "09:33", "09:34", "09:35", "09:36", "09:37", "09:38", "09:39", "09:40", "09:41", "09:42", "09:43", "09:44", "09:45", "09:46", "09:47", "09:48", "09:49", "09:50", "09:51", "09:52", "09:53", "09:54", "09:56", "09:57", "09:58", "09:59", "10:00", "10:01", "10:02", "10:03", "10:04", "10:05", "10:06", "10:07", "10:08", "10:09", "10:10", "10:11", "10:12", "10:13", "10:14", "10:15", "10:16", "10:17", "10:18", "10:19", "10:20", "10:21", "10:22", "10:23", "10:24", "10:25", "10:26", "10:27", "10:28", "10:29", "10:30", "10:31", "10:32", "10:33", "10:34", "10:35", "10:36", "10:37", "10:38", "10:39", "10:40", "10:41", "10:42", "10:43", "10:44", "10:45", "10:46", "10:47", "10:48", "10:49", "10:50", "10:51", "10:52", "10:53", "10:54", "10:56", "10:57", "10:58", "10:59", "11:00", "11:01", "11:02", "11:03", "11:04", "11:05", "11:06", "11:07", "11:08", "11:09", "11:10", "11:11", "11:12", "11:13", "11:14", "11:15", "11:16", "11:17", "11:18", "11:19", "11:20", "11:21", "11:22", "11:23", "11:24", "11:25", "11:26", "11:27", "11:28", "11:29", "11:30", "11:31", "11:32", "11:33", "11:34", "11:35", "11:36", "11:37", "11:38", "11:39", "11:40", "11:41", "11:42", "11:43", "11:44", "11:45", "11:46", "11:47", "11:48", "11:49", "11:50", "11:51", "11:52", "11:53", "11:54", "11:56", "11:57", "11:58", "11:59", "12:00", "12:01", "12:02", "12:03", "12:04", "12:05", "12:06", "12:07", "12:08", "12:09", "12:10", "12:11", "12:12", "12:13", "12:14", "12:15", "12:16", "12:17", "12:18", "12:19", "12:20", "12:21", "12:22", "12:23", "12:24", "12:25", "12:26", "12:27", "12:28", "12:29", "12:30", "12:31", "12:32", "12:33", "12:34", "12:35", "12:36", "12:37", "12:38", "12:39", "12:40", "12:41", "12:42", "12:43", "12:44", "12:45", "12:46", "12:47", "12:48", "12:49", "12:50", "12:51", "12:52", "12:53", "12:54", "12:56", "12:57", "12:58", "12:59", "13:00", "13:01", "13:02", "13:03", "13:04", "13:05", "13:06", "13:07", "13:08", "13:09", "13:10", "13:11", "13:12", "13:13", "13:14", "13:15", "13:16", "13:17", "13:18", "13:19", "13:20", "13:21", "13:22", "13:23", "13:24", "13:25", "13:26", "13:27", "13:28", "13:29", "13:30", "13:31", "13:32", "13:33", "13:34", "13:35", "13:36", "13:37", "13:38", "13:39", "13:40", "13:41", "13:42", "13:43", "13:44", "13:45", "13:46", "13:47", "13:48", "13:49", "13:50", "13:51", "13:52", "13:53", "13:54", "13:56", "13:57", "13:58", "13:59", "14:00", "14:01", "14:02", "14:03", "14:04", "14:05", "14:06", "14:07", "14:08", "14:09", "14:10", "14:11", "14:12", "14:13", "14:14", "14:15", "14:16", "14:17", "14:18", "14:19", "14:20", "14:21", "14:22", "14:23", "14:24", "14:25", "14:26", "14:27", "14:28", "14:29", "14:30", "14:31", "14:32", "14:33", "14:34", "14:35", "14:36", "14:37", "14:38", "14:39", "14:40", "14:41", "14:42", "14:43", "14:44", "14:45", "14:46", "14:47", "14:48", "14:49", "14:50", "14:51", "14:52", "14:53", "14:54", "14:56", "14:57", "14:58", "14:59", "15:00", "15:01", "15:02", "15:03", "15:04", "15:05", "15:06", "15:07", "15:08", "15:09", "15:10", "15:11", "15:12", "15:13", "15:14", "15:15", "15:16", "15:17", "15:18", "15:19", "15:20", "15:21", "15:22", "15:23", "15:24", "15:25", "15:26", "15:27", "15:28", "15:29", "15:30", "15:31", "15:32", "15:33", "15:34", "15:35", "15:36", "15:37", "15:38", "15:39", "15:40", "15:41", "15:42", "15:43", "15:44", "15:45", "15:46", "15:47", "15:48", "15:49", "15:50", "15:51", "15:52", "15:53", "15:54", "15:56", "15:57", "15:58", "15:59", "16:00", "16:01", "16:02", "16:03", "16:04", "16:05", "16:06", "16:07", "16:08", "16:09", "16:10", "16:11", "16:12", "16:13", "16:14", "16:15", "16:16", "16:17", "16:18", "16:19", "16:20", "16:21", "16:22", "16:23", "16:24", "16:25", "16:26", "16:27", "16:28", "16:29", "16:30", "16:31", "16:32", "16:33", "16:34", "16:35", "16:36", "16:37", "16:38", "16:39", "16:40", "16:41", "16:42", "16:43", "16:44", "16:45", "16:46", "16:47", "16:48", "16:49", "16:50", "16:51", "16:52", "16:53", "16:54", "16:56", "16:57", "16:58", "16:59", "17:00", "17:01", "17:02", "17:03", "17:04", "17:05", "17:06", "17:07", "17:08", "17:09", "17:10", "17:11", "17:12", "17:13", "17:14", "17:15", "17:16", "17:17", "17:18", "17:19", "17:20", "17:21", "17:22", "17:23", "17:24", "17:25", "17:26", "17:27", "17:28", "17:29", "17:30", "17:31", "17:32", "17:33", "17:34", "17:35", "17:36", "17:37", "17:38", "17:39", "17:40", "17:41", "17:42", "17:43", "17:44", "17:45", "17:46", "17:47", "17:48", "17:49", "17:50", "17:51", "17:52", "17:53", "17:54", "17:56", "17:57", "17:58", "17:59", "18:00", "18:01", "18:02", "18:03", "18:04", "18:05", "18:06", "18:07", "18:08", "18:09", "18:10", "18:11", "18:12", "18:13", "18:14", "18:15", "18:16", "18:17", "18:18", "18:19", "18:20", "18:21", "18:22", "18:23", "18:24", "18:25", "18:26", "18:27", "18:28", "18:29", "18:30", "18:31", "18:32", "18:33", "18:34", "18:35", "18:36", "18:37", "18:38", "18:39", "18:40", "18:41", "18:42", "18:43", "18:44", "18:45", "18:46", "18:47", "18:48", "18:49", "18:50", "18:51", "18:52", "18:53", "18:54", "18:56", "18:57", "18:58", "18:59", "19:00", "19:01", "19:02", "19:03", "19:04", "19:05", "19:06", "19:07", "19:08", "19:09", "19:10", "19:11", "19:12", "19:13", "19:14", "19:15", "19:16", "19:17", "19:18", "19:19", "19:20", "19:21", "19:22", "19:23", "19:24", "19:25", "19:26", "19:27", "19:28", "19:29", "19:30", "19:31", "19:32", "19:33", "19:34", "19:35", "19:36", "19:37", "19:38", "19:39", "19:40", "19:41", "19:42", "19:43", "19:44", "19:45", "19:46", "19:47", "19:48", "19:49", "19:50", "19:51", "19:52", "19:53", "19:54", "19:56", "19:57", "19:58", "19:59", "20:00"}));
            jPanel7.add(spinnerHoraDeInicioDeLaFuncionDCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 220, 400, 20));

            spinnerPorcentajeBoletoAdultoDCF.setModel(new javax.swing.SpinnerListModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"}));
            jPanel7.add(spinnerPorcentajeBoletoAdultoDCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 280, 380, -1));

            spinnerPorcentajeBoletoNiñoDCF.setModel(new javax.swing.SpinnerListModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"}));
            jPanel7.add(spinnerPorcentajeBoletoNiñoDCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 310, 380, 20));

            spinnerPorcentajeBoletoAdultoMayorDCF.setModel(new javax.swing.SpinnerListModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"}));
            jPanel7.add(spinnerPorcentajeBoletoAdultoMayorDCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 340, 380, -1));

            spinnerPorcentajeBoletoDiscapacitadoDCF.setModel(new javax.swing.SpinnerListModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"}));
            jPanel7.add(spinnerPorcentajeBoletoDiscapacitadoDCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 370, 380, -1));

            txfCostoDeLaEntradaDCF.setText("8");
            jPanel7.add(txfCostoDeLaEntradaDCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 250, 380, -1));

            jLabel44.setText("%");
            jPanel7.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 370, 20, -1));

            jLabel45.setText("$");
            jPanel7.add(jLabel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 250, 20, -1));

            jLabel46.setText("%");
            jPanel7.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 280, 20, -1));

            jLabel47.setText("%");
            jPanel7.add(jLabel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 310, 20, -1));

            jLabel48.setText("%");
            jPanel7.add(jLabel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 340, 20, -1));

            jLabel50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel50.setText("Detalles de la función:");
            jPanel7.add(jLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));

            tabla4DetallesDeLaPeliculaDCF.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {
                    "Nombre / Titulo", "Sinópsis", "Duración", "Dimensión", "Categoría", "Clasificación"
                }
            ));
            jScrollPane6.setViewportView(tabla4DetallesDeLaPeliculaDCF);

            jPanel7.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 970, 40));

            btnCrearFuncionDCF.setText("Crear función");
            btnCrearFuncionDCF.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCrearFuncionDCFActionPerformed(evt);
                }
            });
            jPanel7.add(btnCrearFuncionDCF, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 280, 160, -1));

            jLabel76.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
            jLabel76.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel76MouseClicked(evt);
                }
            });
            jPanel7.add(jLabel76, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

            background7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel7.add(background7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            GuiDetallesDeLaCreacionDeFunciones.getContentPane().add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            GuiMenuPrincipalDelCliente.setUndecorated(true);
            GuiMenuPrincipalDelCliente.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel49.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
            jLabel49.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel49MouseClicked(evt);
                }
            });
            jPanel8.add(jLabel49, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

            jLabel51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
            jLabel51.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel51MouseClicked(evt);
                }
            });
            jPanel8.add(jLabel51, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

            jLabel52.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
            jLabel52.setText("¿Qué desea hacer?");
            jPanel8.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 10, -1, -1));

            jButton1.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
            jButton1.setForeground(new java.awt.Color(255, 0, 0));
            jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/portadasDePeliculas/cinema_ticket_50px.png"))); // NOI18N
            jButton1.setText("Comprar boletos");
            jButton1.setContentAreaFilled(false);
            jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            jButton1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton1ActionPerformed(evt);
                }
            });
            jPanel8.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 270, 210, 70));

            jLabel77.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
            jLabel77.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel77MouseClicked(evt);
                }
            });
            jPanel8.add(jLabel77, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

            background8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel8.add(background8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            GuiMenuPrincipalDelCliente.getContentPane().add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            GuiCompraDeBoletosSeleccionDeFuncion.setUndecorated(true);
            GuiCompraDeBoletosSeleccionDeFuncion.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
            jLabel53.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel53MouseClicked(evt);
                }
            });
            jPanel9.add(jLabel53, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

            jLabel54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
            jLabel54.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel54MouseClicked(evt);
                }
            });
            jPanel9.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

            jLabel55.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
            jLabel55.setText("¿Qué película desea ver?");
            jPanel9.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, -1, -1));

            jLabel58.setText("Categoría:");
            jPanel9.add(jLabel58, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 110, -1, -1));

            jLabel59.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
            jLabel59.setText("Filtrar por:");
            jPanel9.add(jLabel59, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, -1, -1));

            jLabel60.setText("Día:");
            jPanel9.add(jLabel60, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 110, -1, -1));

            comboFiltroPorDimensionCDB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2D", "3D", "4D" }));
            comboFiltroPorDimensionCDB.setEnabled(false);
            comboFiltroPorDimensionCDB.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboFiltroPorDimensionCDBActionPerformed(evt);
                }
            });
            jPanel9.add(comboFiltroPorDimensionCDB, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 110, -1, -1));

            comboFitroPorCategoriaCDB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione una categoría", "Acción", "Aventura", "Terror", "Comedia", "Familiar", "Infantil", "Drama", "Ciencia ficción", "Suspenso", "Romance" }));
            comboFitroPorCategoriaCDB.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboFitroPorCategoriaCDBActionPerformed(evt);
                }
            });
            jPanel9.add(comboFitroPorCategoriaCDB, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 110, -1, -1));

            tabla6PeliculasFiltradasCDB.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {

                }
            ));
            tabla6PeliculasFiltradasCDB.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tabla6PeliculasFiltradasCDBMouseClicked(evt);
                }
            });
            jScrollPane5.setViewportView(tabla6PeliculasFiltradasCDB);

            jPanel9.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, -1, 350));

            tabla7PeliculasFiltradasCDB.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {

                }
            ));
            tabla7PeliculasFiltradasCDB.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tabla7PeliculasFiltradasCDBMouseClicked(evt);
                }
            });
            jScrollPane7.setViewportView(tabla7PeliculasFiltradasCDB);

            jPanel9.add(jScrollPane7, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 140, -1, 350));

            jLabel61.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel61.setText("Selección de película");
            jPanel9.add(jLabel61, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, -1, -1));

            comboFiltroPorDiaCDB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo" }));
            comboFiltroPorDiaCDB.setEnabled(false);
            comboFiltroPorDiaCDB.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboFiltroPorDiaCDBActionPerformed(evt);
                }
            });
            jPanel9.add(comboFiltroPorDiaCDB, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 110, 70, -1));

            jLabel62.setText("Dimensión:");
            jPanel9.add(jLabel62, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 110, -1, -1));

            jLabel63.setText("Sección del día:");
            jPanel9.add(jLabel63, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 110, -1, -1));

            comboFiltroPorSeccionDelDiaCDB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Matutino", "Vespertino" }));
            comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
            comboFiltroPorSeccionDelDiaCDB.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboFiltroPorSeccionDelDiaCDBActionPerformed(evt);
                }
            });
            jPanel9.add(comboFiltroPorSeccionDelDiaCDB, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 110, 90, -1));

            jLabel56.setText("Hora:");
            jPanel9.add(jLabel56, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 110, -1, -1));

            comboFiltroPorHoraCDB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
            comboFiltroPorHoraCDB.setEnabled(false);
            comboFiltroPorHoraCDB.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboFiltroPorHoraCDBActionPerformed(evt);
                }
            });
            jPanel9.add(comboFiltroPorHoraCDB, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 110, -1, -1));

            jLabel78.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
            jLabel78.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel78MouseClicked(evt);
                }
            });
            jPanel9.add(jLabel78, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

            background9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel9.add(background9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            GuiCompraDeBoletosSeleccionDeFuncion.getContentPane().add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            GuiDetallesDeLaFuncion.setUndecorated(true);
            GuiDetallesDeLaFuncion.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jPanel10.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel57.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
            jLabel57.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel57MouseClicked(evt);
                }
            });
            jPanel10.add(jLabel57, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

            jLabel64.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
            jLabel64.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel64MouseClicked(evt);
                }
            });
            jPanel10.add(jLabel64, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

            jLabel65.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
            jLabel65.setText("Detalles de la función");
            jPanel10.add(jLabel65, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 10, -1, -1));

            lblNombreDeLaPeliculaDF.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            lblNombreDeLaPeliculaDF.setText("Titulo de la película");
            jPanel10.add(lblNombreDeLaPeliculaDF, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));
            jPanel10.add(lblPortadaDeLaPeliculaDF, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, -1, -1));

            jLabel66.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel66.setText("Sinopsis:");
            jPanel10.add(jLabel66, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 130, -1, -1));

            lblSinopsisDeLaPeliculaDF.setText("jLabel67");
            jPanel10.add(lblSinopsisDeLaPeliculaDF, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 160, 680, -1));

            jLabel68.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel68.setText("Duración:");
            jPanel10.add(jLabel68, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 190, -1, -1));

            lblDuracionDeLaPeliculaDF.setText("jLabel69");
            jPanel10.add(lblDuracionDeLaPeliculaDF, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 220, 680, -1));

            jLabel70.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel70.setText("Categoría:");
            jPanel10.add(jLabel70, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 250, -1, -1));

            lblCategoriaDeLaPeliculaDF.setText("jLabel71");
            jPanel10.add(lblCategoriaDeLaPeliculaDF, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 280, 680, -1));

            jLabel72.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel72.setText("Clasificación:");
            jPanel10.add(jLabel72, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 310, -1, -1));

            lblClasificacionDeLaPeliculaDF.setText("jLabel73");
            jPanel10.add(lblClasificacionDeLaPeliculaDF, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 340, 680, -1));

            jLabel74.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel74.setText("Costo de los boletos:");
            jPanel10.add(jLabel74, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 370, -1, -1));

            tabla8CostoDeLosBoletosDF.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {
                    "C. B. de adulto", "C. B. de adulto con discapacidad", "C. B. de adulto mayor", "C. B. de adulto mayor con discapacidad", "C. B. de niño", "C. B. de niño con discapacidad"
                }
            ));
            jScrollPane8.setViewportView(tabla8CostoDeLosBoletosDF);

            jPanel10.add(jScrollPane8, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 400, 680, 40));

            btnBuscarOtrasFuncionesDF.setText("Buscar otras funciones");
            btnBuscarOtrasFuncionesDF.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnBuscarOtrasFuncionesDFActionPerformed(evt);
                }
            });
            jPanel10.add(btnBuscarOtrasFuncionesDF, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 460, 470, -1));

            btnComprarBoletosDF.setText("De acuerdo, comprar boletos");
            btnComprarBoletosDF.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnComprarBoletosDFActionPerformed(evt);
                }
            });
            jPanel10.add(btnComprarBoletosDF, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 460, 480, -1));

            jLabel79.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
            jLabel79.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel79MouseClicked(evt);
                }
            });
            jPanel10.add(jLabel79, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

            background10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel10.add(background10, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            GuiDetallesDeLaFuncion.getContentPane().add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            GuiNumeroDeBoletosAComprar.setUndecorated(true);
            GuiNumeroDeBoletosAComprar.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jPanel11.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel80.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
            jLabel80.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel80MouseClicked(evt);
                }
            });
            jPanel11.add(jLabel80, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

            jLabel81.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
            jLabel81.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel81MouseClicked(evt);
                }
            });
            jPanel11.add(jLabel81, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

            jLabel82.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
            jLabel82.setText("¿Cuantos boletos desea comprar?");
            jPanel11.add(jLabel82, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, -1, -1));

            jLabel83.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
            jLabel83.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel83MouseClicked(evt);
                }
            });
            jPanel11.add(jLabel83, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

            btnSeleccionarAsientosNBC.setText("Seleccionar asientos");
            btnSeleccionarAsientosNBC.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnSeleccionarAsientosNBCActionPerformed(evt);
                }
            });
            jPanel11.add(btnSeleccionarAsientosNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 470, 960, -1));

            jLabel85.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel85.setText("Boletos disponibles:");
            jPanel11.add(jLabel85, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, -1, -1));

            txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.setText("0");
            txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfNumeroDeBoletosDeAdultoConDiscapacidadNBCActionPerformed(evt);
                }
            });
            txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    txfNumeroDeBoletosDeAdultoConDiscapacidadNBCKeyTyped(evt);
                }
            });
            jPanel11.add(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 320, 440, -1));

            jLabel86.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel86.setText("Boletos de adulto con discapacidad");
            jPanel11.add(jLabel86, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, -1, -1));

            txfNumeroDeBoletosDeAdolescenteNBC.setText("0");
            txfNumeroDeBoletosDeAdolescenteNBC.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfNumeroDeBoletosDeAdolescenteNBCActionPerformed(evt);
                }
            });
            txfNumeroDeBoletosDeAdolescenteNBC.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    txfNumeroDeBoletosDeAdolescenteNBCKeyTyped(evt);
                }
            });
            jPanel11.add(txfNumeroDeBoletosDeAdolescenteNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 260, 450, -1));

            txfNumeroDeBoletosDeAdultoMayorNBC.setText("0");
            txfNumeroDeBoletosDeAdultoMayorNBC.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfNumeroDeBoletosDeAdultoMayorNBCActionPerformed(evt);
                }
            });
            txfNumeroDeBoletosDeAdultoMayorNBC.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    txfNumeroDeBoletosDeAdultoMayorNBCKeyTyped(evt);
                }
            });
            jPanel11.add(txfNumeroDeBoletosDeAdultoMayorNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 380, 440, -1));

            jLabel87.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel87.setText("Boletos de adulto mayor");
            jPanel11.add(jLabel87, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 350, -1, -1));

            jLabel88.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel88.setText("Boletos de adulto mayor con discapacidad");
            jPanel11.add(jLabel88, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 410, -1, -1));

            txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.setText("0");
            txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfNumeroDeBoletosAdultoMayorConDiscapacidadNBCActionPerformed(evt);
                }
            });
            txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    txfNumeroDeBoletosAdultoMayorConDiscapacidadNBCKeyTyped(evt);
                }
            });
            jPanel11.add(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 440, 440, -1));

            jLabel89.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel89.setText("Boletos de niño");
            jPanel11.add(jLabel89, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 350, -1, -1));

            txfNumeroDeBoletosDeNiñoNBC.setText("0");
            txfNumeroDeBoletosDeNiñoNBC.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfNumeroDeBoletosDeNiñoNBCActionPerformed(evt);
                }
            });
            txfNumeroDeBoletosDeNiñoNBC.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    txfNumeroDeBoletosDeNiñoNBCKeyTyped(evt);
                }
            });
            jPanel11.add(txfNumeroDeBoletosDeNiñoNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 380, 450, -1));

            jLabel90.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel90.setText("Boletos de niño con discapacidad");
            jPanel11.add(jLabel90, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 410, -1, -1));

            txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.setText("0");
            txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfNumeroDeBoletosDeNiñoConDiscapacidadNBCActionPerformed(evt);
                }
            });
            txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    txfNumeroDeBoletosDeNiñoConDiscapacidadNBCKeyTyped(evt);
                }
            });
            jPanel11.add(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 440, 450, -1));

            jLabel96.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel96.setText("Boletos de adolescente (12 a 18 años)");
            jPanel11.add(jLabel96, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 230, -1, -1));

            txfNumeroDeBoletosDeAdultoNBC.setText("0");
            txfNumeroDeBoletosDeAdultoNBC.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfNumeroDeBoletosDeAdultoNBCActionPerformed(evt);
                }
            });
            txfNumeroDeBoletosDeAdultoNBC.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    txfNumeroDeBoletosDeAdultoNBCKeyTyped(evt);
                }
            });
            jPanel11.add(txfNumeroDeBoletosDeAdultoNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 260, 440, -1));

            txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.setText("0");
            txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBCActionPerformed(evt);
                }
            });
            txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBCKeyTyped(evt);
                }
            });
            jPanel11.add(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 320, 450, -1));

            jLabel97.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel97.setText("Boletos de adolescente con discapacidad (12 a 18 años)");
            jPanel11.add(jLabel97, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 290, -1, -1));

            jLabel98.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel98.setText("Boletos de adolescente (12 a 18 años)");
            jPanel11.add(jLabel98, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 230, -1, -1));

            jLabel94.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel94.setText("Especificación de número de boletos");
            jPanel11.add(jLabel94, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, -1, -1));

            jLabel91.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel91.setText("Boletos de adulto");
            jPanel11.add(jLabel91, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 230, -1, -1));

            tabla9CostoDeLosBoletosNBC.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {
                    "C. B. de adulto", "C. B. de adulto con discapacidad", "C. B. de adulto mayor", "C. B. de adulto mayor con discapacidad", "C. B. de niño", "C. B. de niño con discapacidad"
                }
            ));
            jScrollPane9.setViewportView(tabla9CostoDeLosBoletosNBC);

            jPanel11.add(jScrollPane9, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 110, 940, 40));

            jLabel92.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel92.setText("Costo de boletos");
            jPanel11.add(jLabel92, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, -1));

            lblBoletosDisponiblesNBC.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            lblBoletosDisponiblesNBC.setText("00");
            jPanel11.add(lblBoletosDisponiblesNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 160, -1, -1));

            jLabel95.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel95.setText("Número de boletos comprados:");
            jPanel11.add(jLabel95, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 160, -1, -1));

            lblNumeroDeBoletosCompradosNBC.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            lblNumeroDeBoletosCompradosNBC.setText("00");
            jPanel11.add(lblNumeroDeBoletosCompradosNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 160, -1, -1));

            jLabel100.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel100.setText("Valor de la compra actual:");
            jPanel11.add(jLabel100, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 160, -1, -1));

            lblValorDeLaCompraActualNBC.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            lblValorDeLaCompraActualNBC.setText("00");
            jPanel11.add(lblValorDeLaCompraActualNBC, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 160, -1, -1));

            jLabel102.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel102.setText("$");
            jPanel11.add(jLabel102, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 160, -1, -1));

            background11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel11.add(background11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            GuiNumeroDeBoletosAComprar.getContentPane().add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            GuiSeleccionDeAsientos.setUndecorated(true);
            GuiSeleccionDeAsientos.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jPanel12.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel84.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
            jLabel84.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel84MouseClicked(evt);
                }
            });
            jPanel12.add(jLabel84, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

            jLabel93.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
            jLabel93.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel93MouseClicked(evt);
                }
            });
            jPanel12.add(jLabel93, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

            jLabel99.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
            jLabel99.setText("Seleccione los asientos que desea ocupar");
            jPanel12.add(jLabel99, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, -1, -1));

            jLabel101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
            jLabel101.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel101MouseClicked(evt);
                }
            });
            jPanel12.add(jLabel101, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

            panelAsientosDeCineSA.setBackground(new java.awt.Color(255, 255, 255));
            panelAsientosDeCineSA.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    panelAsientosDeCineSAMouseClicked(evt);
                }
            });

            javax.swing.GroupLayout panelAsientosDeCineSALayout = new javax.swing.GroupLayout(panelAsientosDeCineSA);
            panelAsientosDeCineSA.setLayout(panelAsientosDeCineSALayout);
            panelAsientosDeCineSALayout.setHorizontalGroup(
                panelAsientosDeCineSALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 410, Short.MAX_VALUE)
            );
            panelAsientosDeCineSALayout.setVerticalGroup(
                panelAsientosDeCineSALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 250, Short.MAX_VALUE)
            );

            jPanel12.add(panelAsientosDeCineSA, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 130, 410, 250));

            jPanel13.setBackground(new java.awt.Color(0, 0, 0));

            jLabel103.setForeground(new java.awt.Color(255, 255, 255));
            jLabel103.setText("PANTALLA");
            jPanel13.add(jLabel103);

            jPanel12.add(jPanel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 90, 410, 20));

            jLabel104.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel104.setText("D");
            jPanel12.add(jLabel104, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 260, -1, -1));

            jLabel105.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel105.setText("A");
            jPanel12.add(jLabel105, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 140, -1, -1));

            jLabel106.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel106.setText("B");
            jPanel12.add(jLabel106, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 180, -1, -1));

            jLabel107.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel107.setText("C");
            jPanel12.add(jLabel107, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 220, -1, -1));

            jLabel109.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel109.setText("E");
            jPanel12.add(jLabel109, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 300, -1, -1));

            jLabel110.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel110.setText("F");
            jPanel12.add(jLabel110, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 340, -1, -1));

            jLabel112.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel112.setText("1    2    3   4    5    6    7    8   9  10");
            jPanel12.add(jLabel112, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 390, 410, -1));

            btnFacturarYPagarSA.setText("Obtener mi factura y pagar");
            btnFacturarYPagarSA.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnFacturarYPagarSAActionPerformed(evt);
                }
            });
            jPanel12.add(btnFacturarYPagarSA, new org.netbeans.lib.awtextra.AbsoluteConstraints(233, 450, 530, -1));

            background12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel12.add(background12, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            GuiSeleccionDeAsientos.getContentPane().add(jPanel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            GuiFacturacion.setUndecorated(true);
            GuiFacturacion.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jPanel14.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel108.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
            jLabel108.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel108MouseClicked(evt);
                }
            });
            jPanel14.add(jLabel108, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

            jLabel111.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
            jLabel111.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel111MouseClicked(evt);
                }
            });
            jPanel14.add(jLabel111, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

            jLabel113.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
            jLabel113.setText("Facturación");
            jPanel14.add(jLabel113, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, -1, -1));

            jLabel114.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_assignment_return_24px.png"))); // NOI18N
            jLabel114.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel114MouseClicked(evt);
                }
            });
            jPanel14.add(jLabel114, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

            jLabel115.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel115.setText("Detalles de factura:");
            jPanel14.add(jLabel115, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, -1, -1));

            jLabel116.setText("Nombre:");
            jPanel14.add(jLabel116, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 130, -1, -1));
            jPanel14.add(txfNombreDelClienteF, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 130, 360, -1));

            jLabel117.setText("Cédula:");
            jPanel14.add(jLabel117, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 130, -1, -1));
            jPanel14.add(txfCedulaDelClienteF, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 130, 400, -1));

            jLabel118.setText("Teléfono:");
            jPanel14.add(jLabel118, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 160, -1, -1));
            jPanel14.add(txfTelefonoDelClienteF, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 160, 360, -1));

            jLabel119.setText("Dirección:");
            jPanel14.add(jLabel119, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 160, -1, -1));

            txfDireccionDelClienteF.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    txfDireccionDelClienteFActionPerformed(evt);
                }
            });
            jPanel14.add(txfDireccionDelClienteF, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 160, 400, -1));

            jLabel120.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel120.setText("Forma de pago:");
            jPanel14.add(jLabel120, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, -1, -1));

            btnFacturarF.setText("Facturar");
            btnFacturarF.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnFacturarFActionPerformed(evt);
                }
            });
            jPanel14.add(btnFacturarF, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, 920, -1));

            jLabel121.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel121.setText("Total:");
            jPanel14.add(jLabel121, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 370, -1, -1));

            lblSubtotalF.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jPanel14.add(lblSubtotalF, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 390, -1, -1));

            jLabel123.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel123.setText("Subtotal:");
            jPanel14.add(jLabel123, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 390, -1, -1));

            jLabel124.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jLabel124.setText("Iva 12%:");
            jPanel14.add(jLabel124, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 410, -1, -1));

            lblIva12F.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jPanel14.add(lblIva12F, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 410, -1, -1));

            lblTotalF.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
            jPanel14.add(lblTotalF, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 370, -1, -1));

            jLabel122.setText("Número:");
            jPanel14.add(jLabel122, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 260, -1, -1));
            jPanel14.add(txfNumeroDeTarjetaF, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 260, 680, -1));

            jLabel125.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
            jLabel125.setText("Datos del cliente:");
            jPanel14.add(jLabel125, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

            jLabel10.setText("Seleccione su forma de pago:");
            jPanel14.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 230, -1, -1));

            jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tarjeta de crédio", "Tarjeta de débito" }));
            jComboBox1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jComboBox1ActionPerformed(evt);
                }
            });
            jPanel14.add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 230, 680, -1));

            background13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel14.add(background13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            GuiFacturacion.getContentPane().add(jPanel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
            setUndecorated(true);
            setSize(new java.awt.Dimension(1000, 500));
            getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_minimize_window_24px.png"))); // NOI18N
            jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel4MouseClicked(evt);
                }
            });
            jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, -1, -1));

            jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/icons8_close_window_24px.png"))); // NOI18N
            jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jLabel3MouseClicked(evt);
                }
            });
            jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 10, -1, -1));

            btnOpenMenuPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/portadasDePeliculas/system_information_96px.png"))); // NOI18N
            btnOpenMenuPrincipal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0)));
            btnOpenMenuPrincipal.setContentAreaFilled(false);
            btnOpenMenuPrincipal.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            btnOpenMenuPrincipal.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnOpenMenuPrincipalActionPerformed(evt);
                }
            });
            jPanel1.add(btnOpenMenuPrincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 220, 140, 90));

            jLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
            jLabel1.setText("Sistema de cine");
            jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, -1, -1));

            jLabel11.setFont(new java.awt.Font("Century", 1, 14)); // NOI18N
            jLabel11.setForeground(new java.awt.Color(153, 0, 0));
            jLabel11.setText("INICIAR SESION");
            jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 320, 140, 20));

            background1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/medios/PantallaVsFinalFinal.png"))); // NOI18N
            jPanel1.add(background1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

            getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void abrirGui(javax.swing.JFrame gui, javax.swing.JFrame gui2) {
        // Gui hace referencia al frame de donde se llama a la accion de abrir
        // Gui2
        gui.setSize(1000, 500);
        gui.setLocationRelativeTo(null);
        gui.setVisible(false);

        gui2.setSize(1000, 500);
        gui2.setLocationRelativeTo(null);
        gui2.setVisible(true);
    }

    private void eliminarAsientosSeleccionados() {
        for (int i = 0; i < toggleBtnAsientos.length; i++) {
            for (int j = 0; j < toggleBtnAsientos[i].length; j++) {
                if (toggleBtnAsientos[i][j].isSelected()) {
                    toggleBtnAsientos[i][j].setSelected(false);
                    toggleBtnAsientos[i][j].setBackground(Color.GREEN);
                    auxFuncionSeleccionada.getSalaDondeSeProyectara()[i][j] = false;
                }
            }
        }
    }

    private void minimizarGui(javax.swing.JFrame gui) {
        gui.setExtendedState(ICONIFIED);
    }

    private void cerrarGui() {
        try {
            ObjectOutputStream fichero;
            fichero = new ObjectOutputStream(new FileOutputStream("archivo.dat"));
            fichero.writeObject(this.cinema);
            fichero.close();
        } catch (Exception e) {
            System.out.println("" + e);
        }
        System.exit(0);
    }

    private void limpiartabla(DefaultTableModel tabla) {
        for (int i = 0; i < tabla.getRowCount(); i++) {
            tabla.removeRow(i);
        }
        for (int i = 0; i < tabla.getRowCount(); i++) {
            tabla.removeRow(i);
        }
        for (int i = 0; i < tabla.getRowCount(); i++) {
            tabla.removeRow(i);
        }
    }

    private void añadirPeliculaALaCartelera(javax.swing.JTable tab1, javax.swing.JTable tab2, HashMap<Integer, Pelicula> map) {
        int n1, n2, n1aux;
        if (map.size() % 2 == 0) {
            n1 = map.size() / 2;
            n2 = map.size() / 2;
        } else {
            n1 = map.size() / 2 + 1;
            n2 = map.size() / 2;
        }
        n1aux = n1;

        tab1.setDefaultRenderer(Object.class, new img());
        tab2.setDefaultRenderer(Object.class, new img());
        String[] titulos = {"Id", "Portada"};
        DefaultTableModel tm1;
        DefaultTableModel tm2;
        tm1 = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        tm2 = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        tab1.setRowHeight(300);
        tab2.setRowHeight(300);

        for (Pelicula j : map.values()) {
            if (n1 != 0) {
                tm1.addRow(new Object[]{
                    j.getIdDePelicula(),
                    new JLabel(new ImageIcon(j.getRutaDePortada()))
                });
                n1--;
            } else {
                break;
            }
        }
        int k = 0;
        for (Pelicula j : map.values()) {
            if (n1aux != 0) {
                n1aux--;
            } else if (n2 != 0) {
                tm2.addRow(new Object[]{
                    j.getIdDePelicula(),
                    new JLabel(new ImageIcon(j.getRutaDePortada()))
                });
                n2--;
            } else {
                break;
            }
        }
        tab1.setModel(tm1);
        tab2.setModel(tm2);
    }

    private void añadirFuncionesALaCartelera(javax.swing.JTable tab1, javax.swing.JTable tab2, HashMap<Integer, Funcion> map) {
        int n1, n2, n1aux;
        if (map.size() % 2 == 0) {
            n1 = map.size() / 2;
            n2 = map.size() / 2;
        } else {
            n1 = map.size() / 2 + 1;
            n2 = map.size() / 2;
        }
        n1aux = n1;

        tab1.setDefaultRenderer(Object.class, new img());
        tab2.setDefaultRenderer(Object.class, new img());
        String[] titulos = {"Id", "Portada"};
        DefaultTableModel tm1;
        DefaultTableModel tm2;
        tm1 = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        tm2 = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        tab1.setRowHeight(300);
        tab2.setRowHeight(300);

        for (Funcion j : map.values()) {
            if (n1 != 0) {
                tm1.addRow(new Object[]{
                    j.getIdDeFuncion(),
                    new JLabel(new ImageIcon(j.getPeliculaAserProyectada().getRutaDePortada()))
                });
                n1--;
            } else {
                break;
            }
        }
        int k = 0;
        for (Funcion j : map.values()) {
            if (n1aux != 0) {
                n1aux--;
            } else if (n2 != 0) {
                tm2.addRow(new Object[]{
                    j.getIdDeFuncion(),
                    new JLabel(new ImageIcon(j.getPeliculaAserProyectada().getRutaDePortada()))
                });
                n2--;
            } else {
                break;
            }
        }
        tab1.setModel(tm1);
        tab2.setModel(tm2);
    }

    private Hora getHora(javax.swing.JSpinner spinner) throws exceptionHoraIncorrecta, exceptionMinutosIncorrectos {
        String[] aux;
        aux = spinner.getValue().toString().split(":");
        return new Hora(Integer.parseInt(aux[0]), Integer.parseInt(aux[1]));
    }

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        // TODO add your handling code here:
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jLabel4MouseClicked

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel3MouseClicked

    private void btnOpenMenuPrincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenMenuPrincipalActionPerformed
        // TODO add your handling code here:
        abrirGui(this, GuiMenuPrincipal);
    }//GEN-LAST:event_btnOpenMenuPrincipalActionPerformed

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        // TODO add your handling code here:
        GuiMenuPrincipal.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jLabel5MouseClicked

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel6MouseClicked

    private void btnOpenVistaDelClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenVistaDelClienteActionPerformed
        // TODO add your handling code here:
        abrirGui(GuiMenuPrincipal, GuiMenuPrincipalDelCliente);
    }//GEN-LAST:event_btnOpenVistaDelClienteActionPerformed

    private void btnOpenVistaDelAdministradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenVistaDelAdministradorActionPerformed
        // TODO add your handling code here:
        abrirGui(GuiMenuPrincipal, GuiMenuPrincipalAdministrador);
    }//GEN-LAST:event_btnOpenVistaDelAdministradorActionPerformed

    private void jLabel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseClicked
        // TODO add your handling code here:
        GuiMenuPrincipalAdministrador.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jLabel7MouseClicked

    private void jLabel8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel8MouseClicked

    private void btnOpenCrearFuncionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenCrearFuncionActionPerformed
        // TODO add your handling code here:
        abrirGui(GuiMenuPrincipalAdministrador, GuiCreacionDeFunciones);
        añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, cinema.getCatalogoDePeliculas());
    }//GEN-LAST:event_btnOpenCrearFuncionActionPerformed

    private void btnOpenRegistrarPeliculaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenRegistrarPeliculaActionPerformed
        // TODO add your handling code here:
        abrirGui(GuiMenuPrincipalAdministrador, GuiRegistroDePeliculas);
    }//GEN-LAST:event_btnOpenRegistrarPeliculaActionPerformed

    private void jLabel13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MouseClicked
        // TODO add your handling code here:
        GuiRegistroDePeliculas.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jLabel13MouseClicked

    private void jLabel14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel14MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel14MouseClicked

    private void txfSinopsisDeLaPeliculaRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfSinopsisDeLaPeliculaRPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfSinopsisDeLaPeliculaRPActionPerformed

    private void comboCategoriaDePeliculaRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategoriaDePeliculaRPActionPerformed
        // TODO add your handling code here:
        switch (comboCategoriaDePeliculaRP.getSelectedIndex()) {
            case 0:
                // code block
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "B",
                    "B15",
                    "C",
                    "D"
                }));
                comboClasificacionDeLaPeliculaRP.setEnabled(true);
                break;
            case 1:
                // code block
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "B",
                    "B15",
                    "C",
                    "D"
                }));
                comboClasificacionDeLaPeliculaRP.setEnabled(true);
                break;
            case 2:
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "B15",
                    "C",
                    "D"
                }));
                comboClasificacionDeLaPeliculaRP.setEnabled(true);
                break;
            case 3:
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "AA",
                    "A",
                    "B",
                    "B15",
                    "C",
                    "D"
                }));
                comboClasificacionDeLaPeliculaRP.setEnabled(true);
                break;
            case 4:
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "A"}));
                comboClasificacionDeLaPeliculaRP.setEnabled(false);
                break;
            case 5:
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "AA"
                }));
                comboClasificacionDeLaPeliculaRP.setEnabled(false);
                break;
            case 6:
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "A",
                    "B",
                    "B15",
                    "C",
                    "D"
                }));
                comboClasificacionDeLaPeliculaRP.setEnabled(true);
                break;
            case 7:
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "A",
                    "B",
                    "B15",
                    "C",
                    "D"
                }));
                comboClasificacionDeLaPeliculaRP.setEnabled(true);
                break;
            case 8:
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "B",
                    "B15",
                    "C",
                    "D"
                }));
                comboClasificacionDeLaPeliculaRP.setEnabled(true);
                break;
            case 9:
                comboClasificacionDeLaPeliculaRP.setModel(new DefaultComboBoxModel<>(new String[]{
                    "A",
                    "B",
                    "B15",
                    "C",
                    "D"
                }));
                comboClasificacionDeLaPeliculaRP.setEnabled(true);
                break;
            default:
            // code block
        }
        switch (comboClasificacionDeLaPeliculaRP.getSelectedItem().toString()) {
            case "AA":
                // code block
                txfClasificacionDePeliculaRP.setText("Películas para todo público que tengan además atractivo infantil y sean comprensibles para niños menores de siete años de edad.");
                break;
            case "A":
                // code block
                txfClasificacionDePeliculaRP.setText("Películas para todo público.");
                break;
            case "B":
                txfClasificacionDePeliculaRP.setText("Películas para adolescentes de 12 años en adelante.");
                break;
            case "B15":
                txfClasificacionDePeliculaRP.setText("Película no recomendable para menores de 15 años de edad.");
                break;
            case "C":
                txfClasificacionDePeliculaRP.setText("Películas para adultos de 18 años en adelante.");
                break;
            case "D":
                txfClasificacionDePeliculaRP.setText("Películas para adultos, con sexo explícito, lenguaje procaz o alto grado de violencia.");
                break;
            default:
            // code block
        }
    }//GEN-LAST:event_comboCategoriaDePeliculaRPActionPerformed

    private void comboClasificacionDeLaPeliculaRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboClasificacionDeLaPeliculaRPActionPerformed
        // TODO add your handling code here:
        switch (comboClasificacionDeLaPeliculaRP.getSelectedItem().toString()) {
            case "AA":
                // code block
                txfClasificacionDePeliculaRP.setText("Películas para todo público que tengan además atractivo infantil y sean comprensibles para niños menores de siete años de edad.");
                break;
            case "A":
                // code block
                txfClasificacionDePeliculaRP.setText("Películas para todo público.");
                break;
            case "B":
                txfClasificacionDePeliculaRP.setText("Películas para adolescentes de 12 años en adelante.");
                break;
            case "B15":
                txfClasificacionDePeliculaRP.setText("Película no recomendable para menores de 15 años de edad.");
                break;
            case "C":
                txfClasificacionDePeliculaRP.setText("Películas para adultos de 18 años en adelante.");
                break;
            case "D":
                txfClasificacionDePeliculaRP.setText("Películas para adultos, con sexo explícito, lenguaje procaz o alto grado de violencia.");
                break;
            default:
            // code block
        }
    }//GEN-LAST:event_comboClasificacionDeLaPeliculaRPActionPerformed

    private void txfClasificacionDePeliculaRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfClasificacionDePeliculaRPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfClasificacionDePeliculaRPActionPerformed

    private void btnSeleccionarPortadaRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarPortadaRPActionPerformed
        // TODO add your handling code here:
        try {
            String ruta;
            ruta = "";
            JFileChooser dlg;
            dlg = new JFileChooser("");
            dlg.setAcceptAllFileFilterUsed(false);
            dlg.setFileSelectionMode(0);
            dlg.setFileFilter(new FileNameExtensionFilter("Archivos .png", "png"));
            dlg.setFileFilter(new FileNameExtensionFilter("Archivos .jpg", "jpg"));
            int option;
            option = dlg.showOpenDialog(this);
            if (option == JFileChooser.APPROVE_OPTION) {
                File f;
                f = dlg.getSelectedFile();
                ruta = f.toString();
            }
            txfRutaDeLaPortadaRP.setText(ruta);
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_btnSeleccionarPortadaRPActionPerformed

    private void btnRegistrarPeliculaRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarPeliculaRPActionPerformed
        // TODO add your handling code here:
        try {
            Pelicula aux;
            aux = this.cinema.registrarPelicula(
                    txfNombreDeLaPeliculaRP.getText(),
                    txfSinopsisDeLaPeliculaRP.getText(),
                    getHora(spinnerDuracionDeLaPeliculaRP),
                    comboClasificacionDeLaPeliculaRP.getSelectedItem().toString(),
                    comboCategoriaDePeliculaRP.getSelectedItem().toString(),
                    comboDimensionDeLaPeliculaRP.getSelectedItem().toString(),
                    txfRutaDeLaPortadaRP.getText()
            );
            if (aux != null) {
                aux.representarEnTabla(tabla1);
                if (JOptionPane.showConfirmDialog(null, "Registro exitoso, ¿Desea guardar un comprobante de su registro?") == 0) {
                    String ruta;
                    ruta = "";
                    do {
                        JFileChooser dlg;
                        dlg = new JFileChooser();
                        int option;
                        option = dlg.showSaveDialog(this);
                        if (option == JFileChooser.APPROVE_OPTION) {
                            File f;
                            f = dlg.getSelectedFile();
                            ruta = f.toString();
                        }
                    } while (JOptionPane.showConfirmDialog(null, "Esta seguro de exportar el archivo a la ubicación: " + ruta) != 0);
                    aux.representarEnPDF(ruta);
                }
            }
        } catch (exceptionHoraIncorrecta | exceptionMinutosIncorrectos | HeadlessException e) {
            System.out.println("" + e);
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Complete todos los campos correctamente");
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_btnRegistrarPeliculaRPActionPerformed

    private void jLabel25MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel25MouseClicked
        // TODO add your handling code here:
        GuiCreacionDeFunciones.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jLabel25MouseClicked

    private void jLabel26MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel26MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel26MouseClicked

    public void modificarComboBoxDeFiltroPorDimensionCF(HashMap<Integer, Pelicula> map, javax.swing.JComboBox<String> combo) {
        Vector<String> aux;
        aux = new Vector<>();
        aux.add("Seleccione una dimensión");
        for (Pelicula j : map.values()) {
            if (aux.size() == 1) {
                aux.add(j.getDimension());
            } else {
                boolean aux2;
                aux2 = false;
                for (int i = 0; i < aux.size(); i++) {
                    if (aux.get(i).equals(j.getDimension())) {
                        aux2 = true;
                    }
                }
                if (aux2) {
                    aux2 = false;
                } else {
                    aux.add(j.getDimension());
                }
            }
        }
        combo.setModel(new DefaultComboBoxModel<>(aux));
        combo.setEnabled(true);
    }

    public void informarDePeliculasNoEncontradasPorElFiltro(javax.swing.JTable t1, javax.swing.JTable t2, DefaultTableModel tt1, DefaultTableModel tt2, javax.swing.JComboBox<String> combo) {
        JOptionPane.showMessageDialog(null, "No hay peliculas registradas");
        tt1 = (DefaultTableModel) t1.getModel();
        tt2 = (DefaultTableModel) t2.getModel();
        limpiartabla(tt1);
        limpiartabla(tt2);
        combo.setModel(new DefaultComboBoxModel<>(new String[]{}));
        combo.setEnabled(false);
    }

    private void comboFitroPorCategoriaCFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFitroPorCategoriaCFActionPerformed
        // TODO add your handling code here:
        switch (comboFitroPorCategoriaCF.getSelectedIndex()) {
            case 1:
                // code block
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Acción");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            case 2:
                // code block
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Aventura");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            case 3:
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Terror");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            case 4:
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Comedia");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            case 5:
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Familiar");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            case 6:
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Infantil");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            case 7:
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Drama");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            case 8:
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Ciencia ficción");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            case 9:
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Suspenso");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            case 10:
                auxFiltroDePeliculasPorCategoria = cinema.filtrarPeliculasPorCategoria(cinema.getCatalogoDePeliculas(), "Romance");
                if (!auxFiltroDePeliculasPorCategoria.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorCategoria);
                    modificarComboBoxDeFiltroPorDimensionCF(auxFiltroDePeliculasPorCategoria, comboFiltrarPeliculaPorDimensionCF);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla2Cartelera1CF, tabla3Cartelera2CF, tabla2, tabla3, comboFiltrarPeliculaPorDimensionCF);
                }
                break;
            default:
                // code block
                tabla2 = (DefaultTableModel) tabla2Cartelera1CF.getModel();
                tabla3 = (DefaultTableModel) tabla3Cartelera2CF.getModel();
                limpiartabla(tabla2);
                limpiartabla(tabla3);
                comboFiltrarPeliculaPorDimensionCF.setModel(new DefaultComboBoxModel<>(new String[]{}));
                comboFiltrarPeliculaPorDimensionCF.setEnabled(false);
        }
    }//GEN-LAST:event_comboFitroPorCategoriaCFActionPerformed

    private void comboFiltrarPeliculaPorDimensionCFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFiltrarPeliculaPorDimensionCFActionPerformed
        // TODO add your handling code here:
        switch (comboFiltrarPeliculaPorDimensionCF.getSelectedItem().toString()) {
            case "2D":
                // code block
                auxFiltroDePeliculasPorDimension = cinema.filtrarPeliculasPorDimension(auxFiltroDePeliculasPorCategoria, "2D");
                if (!auxFiltroDePeliculasPorDimension.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorDimension);
                } else {
                    JOptionPane.showMessageDialog(null, "No se han encontrado películas registradas");
                }
                break;
            case "3D":
                // code block
                auxFiltroDePeliculasPorDimension = cinema.filtrarPeliculasPorDimension(auxFiltroDePeliculasPorCategoria, "3D");
                if (!auxFiltroDePeliculasPorDimension.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorDimension);
                } else {
                    JOptionPane.showMessageDialog(null, "No se han encontrado películas registradas");
                }
                break;
            case "4D":
                auxFiltroDePeliculasPorDimension = cinema.filtrarPeliculasPorDimension(auxFiltroDePeliculasPorCategoria, "4D");
                if (!auxFiltroDePeliculasPorDimension.isEmpty()) {
                    añadirPeliculaALaCartelera(tabla2Cartelera1CF, tabla3Cartelera2CF, auxFiltroDePeliculasPorDimension);
                } else {
                    JOptionPane.showMessageDialog(null, "No se han encontrado películas registradas");
                }
                break;
            default:
                // code block
                tabla2 = (DefaultTableModel) tabla2Cartelera1CF.getModel();
                tabla3 = (DefaultTableModel) tabla3Cartelera2CF.getModel();
                limpiartabla(tabla2);
                limpiartabla(tabla3);
        }
    }//GEN-LAST:event_comboFiltrarPeliculaPorDimensionCFActionPerformed

    private void tabla2Cartelera1CFMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabla2Cartelera1CFMouseClicked
        // TODO add your handling code here:
        try {
            auxPeliculaSeleccionada = cinema.getCatalogoDePeliculas().get(Integer.parseInt(tabla2Cartelera1CF.getValueAt(tabla2Cartelera1CF.getSelectedRow(), 0).toString()));
            abrirGui(GuiCreacionDeFunciones, GuiDetallesDeLaCreacionDeFunciones);
            auxPeliculaSeleccionada.representarEnTabla(tabla4);
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_tabla2Cartelera1CFMouseClicked

    private void tabla3Cartelera2CFMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabla3Cartelera2CFMouseClicked
        // TODO add your handling code here:
        try {
            auxPeliculaSeleccionada = cinema.getCatalogoDePeliculas().get(Integer.parseInt(tabla3Cartelera2CF.getValueAt(tabla3Cartelera2CF.getSelectedRow(), 0).toString()));
            abrirGui(GuiCreacionDeFunciones, GuiDetallesDeLaCreacionDeFunciones);
            auxPeliculaSeleccionada.representarEnTabla(tabla4);
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_tabla3Cartelera2CFMouseClicked

    private void jLabel32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel32MouseClicked
        // TODO add your handling code here:
        GuiDetallesDeLaCreacionDeFunciones.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jLabel32MouseClicked

    private void jLabel33MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel33MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel33MouseClicked

    private void btnCrearFuncionDCFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearFuncionDCFActionPerformed
        // TODO add your handling code here:
        try {
            Funcion aux;
            aux = cinema.registrarFuncion(
                    auxPeliculaSeleccionada,
                    getHora(spinnerHoraDeInicioDeLaFuncionDCF),
                    Double.parseDouble(txfCostoDeLaEntradaDCF.getText()),
                    Double.parseDouble(spinnerPorcentajeBoletoAdultoDCF.getValue().toString()),
                    Double.parseDouble(spinnerPorcentajeBoletoNiñoDCF.getValue().toString()),
                    Double.parseDouble(spinnerPorcentajeBoletoDiscapacitadoDCF.getValue().toString()),
                    Double.parseDouble(spinnerPorcentajeBoletoAdultoMayorDCF.getValue().toString()),
                    comboDiaDeLaFuncionDCF.getSelectedItem().toString()
            );
            if (aux != null) {
                aux.representarEnTabla(tabla5);
                if (JOptionPane.showConfirmDialog(null, "Registro exitoso, ¿Desea guardar un comprobante de su registro?") == 0) {
                    String ruta;
                    ruta = "";
                    do {
                        JFileChooser dlg;
                        dlg = new JFileChooser();
                        int option;
                        option = dlg.showSaveDialog(this);
                        if (option == JFileChooser.APPROVE_OPTION) {
                            File f;
                            f = dlg.getSelectedFile();
                            ruta = f.toString();
                        }
                    } while (JOptionPane.showConfirmDialog(null, "Esta seguro de exportar el archivo a la ubicación: " + ruta) != 0);
                    aux.representarEnPDF(ruta);
                }
            }
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_btnCrearFuncionDCFActionPerformed

    private void jLabel49MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel49MouseClicked
        // TODO add your handling code here:
        GuiMenuPrincipalDelCliente.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jLabel49MouseClicked

    private void jLabel51MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel51MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel51MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        abrirGui(GuiMenuPrincipalDelCliente, GuiCompraDeBoletosSeleccionDeFuncion);
        añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, cinema.getCatalogoDeFunciones());
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tabla7PeliculasFiltradasCDBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabla7PeliculasFiltradasCDBMouseClicked
        // TODO add your handling code here:
        auxFuncionSeleccionada = cinema.getCatalogoDeFunciones().get(Integer.parseInt(tabla7PeliculasFiltradasCDB.getValueAt(tabla7PeliculasFiltradasCDB.getSelectedRow(), 0).toString()));
        abrirGui(GuiCompraDeBoletosSeleccionDeFuncion, GuiDetallesDeLaFuncion);
        lblNombreDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getNombre());
        lblPortadaDeLaPeliculaDF.setIcon(new ImageIcon(auxFuncionSeleccionada.getPeliculaAserProyectada().getRutaDePortada()));
        lblSinopsisDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getSinopsis());
        lblDuracionDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getDuracion().toString());
        lblCategoriaDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getCategoriaDePelicula());
        lblClasificacionDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getClasificacion());
        auxFuncionSeleccionada.representarCostoDeBoletosEnTabla(tabla8);
    }//GEN-LAST:event_tabla7PeliculasFiltradasCDBMouseClicked

    private void tabla6PeliculasFiltradasCDBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabla6PeliculasFiltradasCDBMouseClicked
        // TODO add your handling code here:
        auxFuncionSeleccionada = cinema.getCatalogoDeFunciones().get(Integer.parseInt(tabla6PeliculasFiltradasCDB.getValueAt(tabla6PeliculasFiltradasCDB.getSelectedRow(), 0).toString()));
        abrirGui(GuiCompraDeBoletosSeleccionDeFuncion, GuiDetallesDeLaFuncion);
        lblNombreDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getNombre());
        lblPortadaDeLaPeliculaDF.setIcon(new ImageIcon(auxFuncionSeleccionada.getPeliculaAserProyectada().getRutaDePortada()));
        lblSinopsisDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getSinopsis());
        lblDuracionDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getDuracion().toString());
        lblCategoriaDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getCategoriaDePelicula());
        lblClasificacionDeLaPeliculaDF.setText(auxFuncionSeleccionada.getPeliculaAserProyectada().getClasificacion());
        auxFuncionSeleccionada.representarCostoDeBoletosEnTabla(tabla8);
    }//GEN-LAST:event_tabla6PeliculasFiltradasCDBMouseClicked

    public void modificarComboBoxDeFiltroPorDiaCDB(HashMap<Integer, Funcion> map, javax.swing.JComboBox<String> combo, javax.swing.JComboBox<String> combo2, javax.swing.JComboBox<String> combo3, javax.swing.JComboBox<String> combo4) {
        Vector<String> aux;
        aux = new Vector<>();
        aux.add("Seleccione una dimensión");
        for (Funcion j : map.values()) {
            if (aux.size() == 1) {
                aux.add(j.getDia());
            } else {
                boolean aux2;
                aux2 = false;
                for (int i = 0; i < aux.size(); i++) {
                    if (aux.get(i).equals(j.getDia())) {
                        aux2 = true;
                    }
                }
                if (aux2) {
                    aux2 = false;
                } else {
                    aux.add(j.getDia());
                }
            }
        }
        combo.setModel(new DefaultComboBoxModel<>(aux));
        combo.setEnabled(true);
        combo2.setEnabled(false);
        combo3.setEnabled(false);
        combo4.setEnabled(false);
    }

    private void comboFitroPorCategoriaCDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFitroPorCategoriaCDBActionPerformed
        // TODO add your handling code here:
//        Seleccione una categoría
//Acción
//Aventura
//Terror
//Comedia
//Familiar
//Infantil
//Drama
//Ciencia ficción
//Suspenso
//Romance
        switch (comboFitroPorCategoriaCDB.getSelectedIndex()) {
            case 1:
                // code block
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Acción");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case 2:
                // code block
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Aventura");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case 3:
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Terror");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case 4:
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Comedia");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case 5:
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Familiar");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case 6:
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Infantil");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case 7:
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Drama");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case 8:
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Ciencia ficción");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case 9:
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Suspenso");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case 10:
                auxFiltroDeFuncionesPorCategoria = cinema.filtrarFuncionesPorCategoria(cinema.getCatalogoDeFunciones(), "Romance");
                if (!auxFiltroDeFuncionesPorCategoria.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorCategoria);
                    modificarComboBoxDeFiltroPorDiaCDB(auxFiltroDeFuncionesPorCategoria, comboFiltroPorDiaCDB, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDiaCDB);
                    comboFiltroPorDimensionCDB.setEnabled(false);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            default:
                // code block
                tabla6 = (DefaultTableModel) tabla6PeliculasFiltradasCDB.getModel();
                tabla7 = (DefaultTableModel) tabla7PeliculasFiltradasCDB.getModel();
                limpiartabla(tabla6);
                limpiartabla(tabla7);
                comboFiltroPorDiaCDB.setModel(new DefaultComboBoxModel<>(new String[]{}));
                comboFiltroPorDiaCDB.setEnabled(false);
                comboFiltroPorDimensionCDB.setEnabled(false);
                comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                comboFiltroPorHoraCDB.setEnabled(false);
        }
    }//GEN-LAST:event_comboFitroPorCategoriaCDBActionPerformed

    public void modificarComboBoxDeFiltroPorSeccionDelDiaCDB(HashMap<Integer, Funcion> map, javax.swing.JComboBox<String> combo, javax.swing.JComboBox<String> combo2) {
        Vector<String> aux;
        aux = new Vector<>();
        aux.add("Seleccione una dimensión");
        for (Funcion j : map.values()) {
            if (aux.size() == 1) {
                aux.add(j.getSeccionDelDia());
            } else {
                boolean aux2;
                aux2 = false;
                for (int i = 0; i < aux.size(); i++) {
                    if (aux.get(i).equals(j.getSeccionDelDia())) {
                        aux2 = true;
                    }
                }
                if (aux2) {
                    aux2 = false;
                } else {
                    aux.add(j.getSeccionDelDia());
                }
            }
        }
        combo.setModel(new DefaultComboBoxModel<>(aux));
        combo.setEnabled(true);
        combo2.setEnabled(false);
    }

    private void comboFiltroPorDimensionCDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFiltroPorDimensionCDBActionPerformed
        // TODO add your handling code here:
        switch (comboFiltroPorDimensionCDB.getSelectedItem().toString()) {
            case "2D":
                // code block
                auxFiltroDeFuncionesPorDimension = cinema.filtrarFuncionesPorDimension(auxFiltroDeFuncionesPorDia, "2D");
                if (!auxFiltroDeFuncionesPorDimension.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDimension);
                    modificarComboBoxDeFiltroPorSeccionDelDiaCDB(auxFiltroDeFuncionesPorDimension, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorSeccionDelDiaCDB);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case "3D":
                // code block
                auxFiltroDeFuncionesPorDimension = cinema.filtrarFuncionesPorDimension(auxFiltroDeFuncionesPorDia, "3D");
                if (!auxFiltroDeFuncionesPorDimension.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDimension);
                    modificarComboBoxDeFiltroPorSeccionDelDiaCDB(auxFiltroDeFuncionesPorDimension, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorSeccionDelDiaCDB);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case "4D":
                auxFiltroDeFuncionesPorDimension = cinema.filtrarFuncionesPorDimension(auxFiltroDeFuncionesPorDia, "4D");
                if (!auxFiltroDeFuncionesPorDimension.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDimension);
                    modificarComboBoxDeFiltroPorSeccionDelDiaCDB(auxFiltroDeFuncionesPorDimension, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorSeccionDelDiaCDB);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            default:
                tabla6 = (DefaultTableModel) tabla6PeliculasFiltradasCDB.getModel();
                tabla7 = (DefaultTableModel) tabla7PeliculasFiltradasCDB.getModel();
                limpiartabla(tabla6);
                limpiartabla(tabla7);
                comboFiltroPorSeccionDelDiaCDB.setModel(new DefaultComboBoxModel<>(new String[]{}));
                comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                comboFiltroPorHoraCDB.setEnabled(false);
            // code block
        }
    }//GEN-LAST:event_comboFiltroPorDimensionCDBActionPerformed

    private void jLabel54MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel54MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel54MouseClicked

    private void jLabel53MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel53MouseClicked
        // TODO add your handling code here:
        GuiCompraDeBoletosSeleccionDeFuncion.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jLabel53MouseClicked

    public void modificarComboBoxDeFiltroPorDimensionCDB(HashMap<Integer, Funcion> map, javax.swing.JComboBox<String> combo, javax.swing.JComboBox<String> combo2, javax.swing.JComboBox<String> combo3) {
        Vector<String> aux;
        aux = new Vector<>();
        aux.add("Seleccione una dimensión");
        for (Funcion j : map.values()) {
            if (aux.size() == 1) {
                aux.add(j.getPeliculaAserProyectada().getDimension());
            } else {
                boolean aux2;
                aux2 = false;
                for (int i = 0; i < aux.size(); i++) {
                    if (aux.get(i).equals(j.getPeliculaAserProyectada().getDimension())) {
                        aux2 = true;
                    }
                }
                if (aux2) {
                    aux2 = false;
                } else {
                    aux.add(j.getPeliculaAserProyectada().getDimension());
                }
            }
        }
        combo.setModel(new DefaultComboBoxModel<>(aux));
        combo.setEnabled(true);
        combo2.setEnabled(false);
        combo3.setEnabled(false);
    }

    private void comboFiltroPorDiaCDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFiltroPorDiaCDBActionPerformed
        // TODO add your handling code here:
        switch (comboFiltroPorDiaCDB.getSelectedItem().toString()) {
            case "Lunes":
                // code block
                auxFiltroDeFuncionesPorDia = cinema.filtrarFuncionesPorDia(auxFiltroDeFuncionesPorCategoria, "Lunes");
                if (!auxFiltroDeFuncionesPorDia.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDia);
                    modificarComboBoxDeFiltroPorDimensionCDB(auxFiltroDeFuncionesPorDia, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDimensionCDB);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case "Martes":
                // code block
                auxFiltroDeFuncionesPorDia = cinema.filtrarFuncionesPorDia(auxFiltroDeFuncionesPorCategoria, "Martes");
                if (!auxFiltroDeFuncionesPorDia.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDia);
                    modificarComboBoxDeFiltroPorDimensionCDB(auxFiltroDeFuncionesPorDia, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDimensionCDB);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case "Miércoles":
                auxFiltroDeFuncionesPorDia = cinema.filtrarFuncionesPorDia(auxFiltroDeFuncionesPorCategoria, "Miércoles");
                if (!auxFiltroDeFuncionesPorDia.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDia);
                    modificarComboBoxDeFiltroPorDimensionCDB(auxFiltroDeFuncionesPorDia, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDimensionCDB);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case "Jueves":
                auxFiltroDeFuncionesPorDia = cinema.filtrarFuncionesPorDia(auxFiltroDeFuncionesPorCategoria, "Jueves");
                if (!auxFiltroDeFuncionesPorDia.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDia);
                    modificarComboBoxDeFiltroPorDimensionCDB(auxFiltroDeFuncionesPorDia, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDimensionCDB);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case "Viernes":
                auxFiltroDeFuncionesPorDia = cinema.filtrarFuncionesPorDia(auxFiltroDeFuncionesPorCategoria, "Viernes");
                if (!auxFiltroDeFuncionesPorDia.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDia);
                    modificarComboBoxDeFiltroPorDimensionCDB(auxFiltroDeFuncionesPorDia, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDimensionCDB);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case "Sábado":
                auxFiltroDeFuncionesPorDia = cinema.filtrarFuncionesPorDia(auxFiltroDeFuncionesPorCategoria, "Sábado");
                if (!auxFiltroDeFuncionesPorDia.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDia);
                    modificarComboBoxDeFiltroPorDimensionCDB(auxFiltroDeFuncionesPorDia, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDimensionCDB);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            case "Domingo":
                auxFiltroDeFuncionesPorDia = cinema.filtrarFuncionesPorDia(auxFiltroDeFuncionesPorCategoria, "Domingo");
                if (!auxFiltroDeFuncionesPorDia.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorDia);
                    modificarComboBoxDeFiltroPorDimensionCDB(auxFiltroDeFuncionesPorDia, comboFiltroPorDimensionCDB, comboFiltroPorSeccionDelDiaCDB, comboFiltroPorHoraCDB);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorDimensionCDB);
                    comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                    comboFiltroPorHoraCDB.setEnabled(false);
                }
                break;
            default:
                // code block
                tabla6 = (DefaultTableModel) tabla6PeliculasFiltradasCDB.getModel();
                tabla7 = (DefaultTableModel) tabla7PeliculasFiltradasCDB.getModel();
                limpiartabla(tabla6);
                limpiartabla(tabla7);
                comboFiltroPorDimensionCDB.setModel(new DefaultComboBoxModel<>(new String[]{}));
                comboFiltroPorDimensionCDB.setEnabled(false);
                comboFiltroPorSeccionDelDiaCDB.setEnabled(false);
                comboFiltroPorHoraCDB.setEnabled(false);
        }
    }//GEN-LAST:event_comboFiltroPorDiaCDBActionPerformed

    public void modificarComboBoxDeFiltroPorHora(HashMap<Integer, Funcion> map, javax.swing.JComboBox<String> combo) {
        Vector<String> aux;
        aux = new Vector<>();
        for (Funcion j : map.values()) {
            if (aux.size() == 0) {
                aux.add(j.getHoraInicio().toString());
            } else {
                boolean aux2;
                aux2 = false;
                for (int i = 0; i < aux.size(); i++) {
                    if (aux.get(i).equals(j.getHoraInicio().toString())) {
                        aux2 = true;
                    }
                }
                if (aux2) {
                    aux2 = false;
                } else {
                    aux.add(j.getHoraInicio().toString());
                }
            }
        }
        combo.setModel(new DefaultComboBoxModel<>(aux));
        combo.setEnabled(true);
    }

    private void comboFiltroPorSeccionDelDiaCDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFiltroPorSeccionDelDiaCDBActionPerformed
        // TODO add your handling code here:
        switch (comboFiltroPorSeccionDelDiaCDB.getSelectedItem().toString()) {
            case "Matutino":
                // code block
                auxFiltroDeFuncionesPorSeccionDelDia = cinema.filtrarFuncionesPorSeccionDelDia(auxFiltroDeFuncionesPorDimension, "Matutino");
                if (!auxFiltroDeFuncionesPorSeccionDelDia.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorSeccionDelDia);
                    modificarComboBoxDeFiltroPorHora(auxFiltroDeFuncionesPorSeccionDelDia, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorHoraCDB);
                }
                break;
            case "Vespertino":
                // code block
                auxFiltroDeFuncionesPorSeccionDelDia = cinema.filtrarFuncionesPorSeccionDelDia(auxFiltroDeFuncionesPorDimension, "Vespertino");
                if (!auxFiltroDeFuncionesPorSeccionDelDia.isEmpty()) {
                    añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorSeccionDelDia);
                    modificarComboBoxDeFiltroPorHora(auxFiltroDeFuncionesPorSeccionDelDia, comboFiltroPorHoraCDB);
                } else {
                    informarDePeliculasNoEncontradasPorElFiltro(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, tabla6, tabla7, comboFiltroPorHoraCDB);
                }
                break;
            default:
                tabla6 = (DefaultTableModel) tabla6PeliculasFiltradasCDB.getModel();
                tabla7 = (DefaultTableModel) tabla7PeliculasFiltradasCDB.getModel();
                limpiartabla(tabla6);
                limpiartabla(tabla7);
                comboFiltroPorHoraCDB.setModel(new DefaultComboBoxModel<>(new String[]{}));
                comboFiltroPorHoraCDB.setEnabled(false);
            // code block
        }
    }//GEN-LAST:event_comboFiltroPorSeccionDelDiaCDBActionPerformed

    private Hora getHora(javax.swing.JComboBox<String> a) throws exceptionHoraIncorrecta, exceptionMinutosIncorrectos {
        String[] aux;
        aux = a.getSelectedItem().toString().split(":");
        return new Hora(Integer.parseInt(aux[0]), Integer.parseInt(aux[1]));
    }

    private void comboFiltroPorHoraCDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFiltroPorHoraCDBActionPerformed
        // TODO add your handling code here:
        try {
            auxFiltroDeFuncionesPorHorario = cinema.filtrarFuncionesPorHora(auxFiltroDeFuncionesPorSeccionDelDia, getHora(comboFiltroPorHoraCDB));
            añadirFuncionesALaCartelera(tabla6PeliculasFiltradasCDB, tabla7PeliculasFiltradasCDB, auxFiltroDeFuncionesPorHorario);
        } catch (Exception e) {
            System.out.println("" + e);
        }


    }//GEN-LAST:event_comboFiltroPorHoraCDBActionPerformed

    private void jLabel57MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel57MouseClicked
        // TODO add your handling code here:
        GuiDetallesDeLaFuncion.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jLabel57MouseClicked

    private void jLabel64MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel64MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel64MouseClicked

    private void jLabel69MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel69MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel69MousePressed

    private void jLabel69MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel69MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiMenuPrincipal, this);
    }//GEN-LAST:event_jLabel69MouseClicked

    private void jLabel71MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel71MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiMenuPrincipalAdministrador, GuiMenuPrincipal);
    }//GEN-LAST:event_jLabel71MouseClicked

    private void jLabel73MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel73MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiRegistroDePeliculas, GuiMenuPrincipalAdministrador);
    }//GEN-LAST:event_jLabel73MouseClicked

    private void jLabel75MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel75MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiCreacionDeFunciones, GuiMenuPrincipalAdministrador);
    }//GEN-LAST:event_jLabel75MouseClicked

    private void jLabel76MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel76MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiDetallesDeLaCreacionDeFunciones, GuiCreacionDeFunciones);
    }//GEN-LAST:event_jLabel76MouseClicked

    private void jLabel77MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel77MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiMenuPrincipalDelCliente, GuiMenuPrincipal);
    }//GEN-LAST:event_jLabel77MouseClicked

    private void jLabel78MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel78MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiCompraDeBoletosSeleccionDeFuncion, GuiMenuPrincipalDelCliente);
    }//GEN-LAST:event_jLabel78MouseClicked

    private void jLabel79MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel79MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiDetallesDeLaFuncion, GuiCompraDeBoletosSeleccionDeFuncion);
    }//GEN-LAST:event_jLabel79MouseClicked

    private void btnBuscarOtrasFuncionesDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarOtrasFuncionesDFActionPerformed
        // TODO add your handling code here:
        abrirGui(GuiDetallesDeLaFuncion, GuiCompraDeBoletosSeleccionDeFuncion);
    }//GEN-LAST:event_btnBuscarOtrasFuncionesDFActionPerformed

    private void jLabel80MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel80MouseClicked
        // TODO add your handling code here:
        minimizarGui(GuiNumeroDeBoletosAComprar);
    }//GEN-LAST:event_jLabel80MouseClicked

    private void jLabel81MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel81MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel81MouseClicked

    private void btnComprarBoletosDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComprarBoletosDFActionPerformed
        // TODO add your handling code here:
        abrirGui(GuiDetallesDeLaFuncion, GuiNumeroDeBoletosAComprar);
        auxFuncionSeleccionada.representarCostoDeBoletosEnTabla(tabla9);
        txfNumeroDeBoletosDeNiñoNBC.setEnabled(auxFuncionSeleccionada.isAptitudParaMenores());
        txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.setEnabled(auxFuncionSeleccionada.isAptitudParaMenores());
        txfNumeroDeBoletosDeAdolescenteNBC.setEnabled(auxFuncionSeleccionada.isAptitudParaAdolescentes());
        txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.setEnabled(auxFuncionSeleccionada.isAptitudParaAdolescentes());
        lblBoletosDisponiblesNBC.setText(auxFuncionSeleccionada.calcularBoletosDisponibles() + "");
    }//GEN-LAST:event_btnComprarBoletosDFActionPerformed

    private void jLabel83MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel83MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiNumeroDeBoletosAComprar, GuiDetallesDeLaFuncion);
    }//GEN-LAST:event_jLabel83MouseClicked

    private void crearEstructuraDeAsientos() {
        int largo, ancho, x, y;
        largo = 30;
        ancho = 30;
        x = 10;
        y = 10;
        this.toggleBtnAsientos = new JToggleButton[6][10];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 10; j++) {
                toggleBtnAsientos[i][j] = new JToggleButton();
                toggleBtnAsientos[i][j].setBounds(x, y, ancho, largo);
                AccionBotones accion;
                accion = new AccionBotones();
                toggleBtnAsientos[i][j].addActionListener(accion);
                panelAsientosDeCineSA.add(toggleBtnAsientos[i][j]);

                x += 40;
            }
            x = 10;
            y += 40;
        }
    }

    private void actualizarAsientos() {
        for (int i = 0; i < toggleBtnAsientos.length; i++) {
            for (int j = 0; j < toggleBtnAsientos[i].length; j++) {
                if (auxFuncionSeleccionada.getSalaDondeSeProyectara()[i][j]) {
                    toggleBtnAsientos[i][j].setBackground(Color.RED);
                    toggleBtnAsientos[i][j].setEnabled(false);
                    toggleBtnAsientos[i][j].setSelected(false);
                } else {
                    toggleBtnAsientos[i][j].setBackground(Color.GREEN);
                    toggleBtnAsientos[i][j].setEnabled(true);
                    toggleBtnAsientos[i][j].setSelected(false);
                }
            }
        }
    }

    private void btnSeleccionarAsientosNBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarAsientosNBCActionPerformed
        // TODO add your handling code here:
        try {
            auxNumeroDeBoletosDeseados = (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()));
            lblNumeroDeBoletosCompradosNBC.setText("" + auxNumeroDeBoletosDeseados);
            if (auxNumeroDeBoletosDeseados <= auxFuncionSeleccionada.calcularBoletosDisponibles()) {
                abrirGui(GuiNumeroDeBoletosAComprar, GuiSeleccionDeAsientos);
                actualizarAsientos();
            } else {
                JOptionPane.showMessageDialog(null, "No hay boletos suficientes disponibles");
            }

        } catch (HeadlessException | NumberFormatException e) {
            System.out.println("" + e);
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Complete los campos correctamente");
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_btnSeleccionarAsientosNBCActionPerformed

    private void txfNumeroDeBoletosDeAdultoNBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdultoNBCActionPerformed
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }

    }//GEN-LAST:event_txfNumeroDeBoletosDeAdultoNBCActionPerformed

    private void txfNumeroDeBoletosDeAdultoConDiscapacidadNBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdultoConDiscapacidadNBCActionPerformed
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeAdultoConDiscapacidadNBCActionPerformed

    private void txfNumeroDeBoletosDeAdultoMayorNBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdultoMayorNBCActionPerformed
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeAdultoMayorNBCActionPerformed

    private void txfNumeroDeBoletosAdultoMayorConDiscapacidadNBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosAdultoMayorConDiscapacidadNBCActionPerformed
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosAdultoMayorConDiscapacidadNBCActionPerformed

    private void txfNumeroDeBoletosDeAdolescenteNBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdolescenteNBCActionPerformed
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeAdolescenteNBCActionPerformed

    private void txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBCActionPerformed
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBCActionPerformed

    private void txfNumeroDeBoletosDeNiñoNBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeNiñoNBCActionPerformed
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeNiñoNBCActionPerformed

    private void txfNumeroDeBoletosDeNiñoConDiscapacidadNBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeNiñoConDiscapacidadNBCActionPerformed
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeNiñoConDiscapacidadNBCActionPerformed

    private void txfNumeroDeBoletosDeAdultoNBCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdultoNBCKeyTyped
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeAdultoNBCKeyTyped

    private void txfNumeroDeBoletosDeAdultoConDiscapacidadNBCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdultoConDiscapacidadNBCKeyTyped
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeAdultoConDiscapacidadNBCKeyTyped

    private void txfNumeroDeBoletosDeAdultoMayorNBCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdultoMayorNBCKeyTyped
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeAdultoMayorNBCKeyTyped

    private void txfNumeroDeBoletosAdultoMayorConDiscapacidadNBCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosAdultoMayorConDiscapacidadNBCKeyTyped
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosAdultoMayorConDiscapacidadNBCKeyTyped

    private void txfNumeroDeBoletosDeAdolescenteNBCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdolescenteNBCKeyTyped
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeAdolescenteNBCKeyTyped

    private void txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBCKeyTyped
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBCKeyTyped

    private void txfNumeroDeBoletosDeNiñoNBCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeNiñoNBCKeyTyped
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeNiñoNBCKeyTyped

    private void txfNumeroDeBoletosDeNiñoConDiscapacidadNBCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfNumeroDeBoletosDeNiñoConDiscapacidadNBCKeyTyped
        // TODO add your handling code here:
        try {
            lblNumeroDeBoletosCompradosNBC.setText(
                    ""
                    + (Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText())
                    + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()))
            );
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_txfNumeroDeBoletosDeNiñoConDiscapacidadNBCKeyTyped

    private void jLabel84MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel84MouseClicked
        // TODO add your handling code here:
        minimizarGui(GuiSeleccionDeAsientos);
    }//GEN-LAST:event_jLabel84MouseClicked

    private void jLabel93MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel93MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel93MouseClicked

    private void jLabel101MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel101MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiSeleccionDeAsientos, GuiNumeroDeBoletosAComprar);
        eliminarAsientosSeleccionados();
    }//GEN-LAST:event_jLabel101MouseClicked

    private void panelAsientosDeCineSAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelAsientosDeCineSAMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_panelAsientosDeCineSAMouseClicked

    private void jLabel108MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel108MouseClicked
        // TODO add your handling code here:
        minimizarGui(GuiFacturacion);
    }//GEN-LAST:event_jLabel108MouseClicked

    private void jLabel111MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel111MouseClicked
        // TODO add your handling code here:
        cerrarGui();
    }//GEN-LAST:event_jLabel111MouseClicked

    private void btnFacturarYPagarSAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacturarYPagarSAActionPerformed
        // TODO add your handling code here:
        try {
            if (auxNumeroDeBoletosDeseados != 0) {
                JOptionPane.showMessageDialog(null, "Por favor seleccione todos los asientos");
            } else {
                double[] aux;
                aux = cinema.estimarCostoDeFactura(auxFuncionSeleccionada,
                        Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText()) + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText()),
                        Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText()) + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()),
                        Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText()),
                        Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText()),
                        Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText()),
                        Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText()),
                        cinema.definirAsientosSeleccionados(toggleBtnAsientos, Integer.parseInt(lblNumeroDeBoletosCompradosNBC.getText())));
                lblTotalF.setText(aux[0]+ "");
                lblSubtotalF.setText(aux[1]+"");
                lblIva12F.setText(aux[2]+"");
                abrirGui(GuiSeleccionDeAsientos, GuiFacturacion);
            }
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }//GEN-LAST:event_btnFacturarYPagarSAActionPerformed

    private void jLabel114MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel114MouseClicked
        // TODO add your handling code here:
        abrirGui(GuiFacturacion, GuiSeleccionDeAsientos);
    }//GEN-LAST:event_jLabel114MouseClicked

    private void txfDireccionDelClienteFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfDireccionDelClienteFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfDireccionDelClienteFActionPerformed

    private void btnFacturarFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacturarFActionPerformed
        // TODO add your handling code here:
        try {
            Factura aux;
            aux = cinema.facturar(
                    auxFuncionSeleccionada,
                    Integer.parseInt(txfNumeroDeBoletosDeAdultoNBC.getText()) + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteNBC.getText()),
                    Integer.parseInt(txfNumeroDeBoletosDeAdultoConDiscapacidadNBC.getText()) + Integer.parseInt(txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC.getText()),
                    Integer.parseInt(txfNumeroDeBoletosDeAdultoMayorNBC.getText()),
                    Integer.parseInt(txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC.getText()),
                    Integer.parseInt(txfNumeroDeBoletosDeNiñoNBC.getText()),
                    Integer.parseInt(txfNumeroDeBoletosDeNiñoConDiscapacidadNBC.getText()),
                    cinema.definirAsientosSeleccionados(toggleBtnAsientos, Integer.parseInt(lblNumeroDeBoletosCompradosNBC.getText())),
                    txfNombreDelClienteF.getText(),
                    txfCedulaDelClienteF.getText(),
                    txfDireccionDelClienteF.getText(),
                    txfTelefonoDelClienteF.getText()
            );
            System.out.println("Facturación correcta");
            if (aux != null) {
                lblTotalF.setText(aux.getTotal() + "");
                lblSubtotalF.setText(aux.getSubTotal() + "");
                lblIva12F.setText("" + aux.getIva());
                String ruta;
                ruta = "";
                if (txfNumeroDeTarjetaF.equals("") || txfNumeroDeTarjetaF.getText().length() <= 9) {
                    JOptionPane.showMessageDialog(null, "Ingrese un número de tarjeta correcto");
                } else {
                    JOptionPane.showInputDialog("Ingrese el CV/S de su tarjeta");
                }
                do {
                    JFileChooser dlg;
                    dlg = new JFileChooser();
                    int option;
                    option = dlg.showSaveDialog(this);
                    if (option == JFileChooser.APPROVE_OPTION) {
                        File f;
                        f = dlg.getSelectedFile();
                        ruta = f.toString();
                    }
                } while (JOptionPane.showConfirmDialog(null, "Está seguro de guardar el archivo en: " + ruta) != 0);
                aux.representarEnPDF(ruta);
                abrirGui(GuiFacturacion, GuiMenuPrincipalDelCliente);
                actualizarAsientos();
                JOptionPane.showMessageDialog(null, "Facturación y compra realizada con éxito");
            } else {
                JOptionPane.showMessageDialog(null, "Factura no generada");
            }
        } catch (HeadlessException | NumberFormatException e) {
            System.out.println("" + e);
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Complete los campos correctamente");
        } catch (Exception e) {
            System.out.println("" + e);
        }

    }//GEN-LAST:event_btnFacturarFActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GuiPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GuiPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GuiPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GuiPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GuiPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFrame GuiCompraDeBoletosSeleccionDeFuncion;
    private javax.swing.JFrame GuiCreacionDeFunciones;
    private javax.swing.JFrame GuiDetallesDeLaCreacionDeFunciones;
    private javax.swing.JFrame GuiDetallesDeLaFuncion;
    private javax.swing.JFrame GuiFacturacion;
    private javax.swing.JFrame GuiMenuPrincipal;
    private javax.swing.JFrame GuiMenuPrincipalAdministrador;
    private javax.swing.JFrame GuiMenuPrincipalDelCliente;
    private javax.swing.JFrame GuiNumeroDeBoletosAComprar;
    private javax.swing.JFrame GuiRegistroDePeliculas;
    private javax.swing.JFrame GuiSeleccionDeAsientos;
    private javax.swing.JLabel background1;
    private javax.swing.JLabel background10;
    private javax.swing.JLabel background11;
    private javax.swing.JLabel background12;
    private javax.swing.JLabel background13;
    private javax.swing.JLabel background2;
    private javax.swing.JLabel background3;
    private javax.swing.JLabel background5;
    private javax.swing.JLabel background6;
    private javax.swing.JLabel background7;
    private javax.swing.JLabel background8;
    private javax.swing.JLabel background9;
    private javax.swing.JButton btnBuscarOtrasFuncionesDF;
    private javax.swing.JButton btnComprarBoletosDF;
    private javax.swing.JButton btnCrearFuncionDCF;
    private javax.swing.JButton btnFacturarF;
    private javax.swing.JButton btnFacturarYPagarSA;
    private javax.swing.JButton btnOpenCrearFuncion;
    private javax.swing.JButton btnOpenMenuPrincipal;
    private javax.swing.JButton btnOpenRegistrarPelicula;
    private javax.swing.JButton btnOpenVistaDelAdministrador;
    private javax.swing.JButton btnOpenVistaDelCliente;
    private javax.swing.JButton btnRegistrarPeliculaRP;
    private javax.swing.JButton btnSeleccionarAsientosNBC;
    private javax.swing.JButton btnSeleccionarPortadaRP;
    private javax.swing.JComboBox<String> comboCategoriaDePeliculaRP;
    private javax.swing.JComboBox<String> comboClasificacionDeLaPeliculaRP;
    private javax.swing.JComboBox<String> comboDiaDeLaFuncionDCF;
    private javax.swing.JComboBox<String> comboDimensionDeLaPeliculaRP;
    private javax.swing.JComboBox<String> comboFiltrarPeliculaPorDimensionCF;
    private javax.swing.JComboBox<String> comboFiltroPorDiaCDB;
    private javax.swing.JComboBox<String> comboFiltroPorDimensionCDB;
    private javax.swing.JComboBox<String> comboFiltroPorHoraCDB;
    private javax.swing.JComboBox<String> comboFiltroPorSeccionDelDiaCDB;
    private javax.swing.JComboBox<String> comboFitroPorCategoriaCDB;
    private javax.swing.JComboBox<String> comboFitroPorCategoriaCF;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel100;
    private javax.swing.JLabel jLabel101;
    private javax.swing.JLabel jLabel102;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel105;
    private javax.swing.JLabel jLabel106;
    private javax.swing.JLabel jLabel107;
    private javax.swing.JLabel jLabel108;
    private javax.swing.JLabel jLabel109;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel110;
    private javax.swing.JLabel jLabel111;
    private javax.swing.JLabel jLabel112;
    private javax.swing.JLabel jLabel113;
    private javax.swing.JLabel jLabel114;
    private javax.swing.JLabel jLabel115;
    private javax.swing.JLabel jLabel116;
    private javax.swing.JLabel jLabel117;
    private javax.swing.JLabel jLabel118;
    private javax.swing.JLabel jLabel119;
    private javax.swing.JLabel jLabel120;
    private javax.swing.JLabel jLabel121;
    private javax.swing.JLabel jLabel122;
    private javax.swing.JLabel jLabel123;
    private javax.swing.JLabel jLabel124;
    private javax.swing.JLabel jLabel125;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JLabel jLabel97;
    private javax.swing.JLabel jLabel98;
    private javax.swing.JLabel jLabel99;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lblBoletosDisponiblesNBC;
    private javax.swing.JLabel lblCategoriaDeLaPeliculaDF;
    private javax.swing.JLabel lblClasificacionDeLaPeliculaDF;
    private javax.swing.JLabel lblDuracionDeLaPeliculaDF;
    private javax.swing.JLabel lblIva12F;
    private javax.swing.JLabel lblNombreDeLaPeliculaDF;
    private javax.swing.JLabel lblNumeroDeBoletosCompradosNBC;
    private javax.swing.JLabel lblPortadaDeLaPeliculaDF;
    private javax.swing.JLabel lblSinopsisDeLaPeliculaDF;
    private javax.swing.JLabel lblSubtotalF;
    private javax.swing.JLabel lblTotalF;
    private javax.swing.JLabel lblValorDeLaCompraActualNBC;
    private javax.swing.JPanel panelAsientosDeCineSA;
    private javax.swing.JSpinner spinnerDuracionDeLaPeliculaRP;
    private javax.swing.JSpinner spinnerHoraDeInicioDeLaFuncionDCF;
    private javax.swing.JSpinner spinnerPorcentajeBoletoAdultoDCF;
    private javax.swing.JSpinner spinnerPorcentajeBoletoAdultoMayorDCF;
    private javax.swing.JSpinner spinnerPorcentajeBoletoDiscapacitadoDCF;
    private javax.swing.JSpinner spinnerPorcentajeBoletoNiñoDCF;
    private javax.swing.JTable tabla1RegistrosDePeliculasRecientesRP;
    private javax.swing.JTable tabla2Cartelera1CF;
    private javax.swing.JTable tabla3Cartelera2CF;
    private javax.swing.JTable tabla4DetallesDeLaPeliculaDCF;
    private javax.swing.JTable tabla5FuncionesRegistradasRecientementeDCF;
    private javax.swing.JTable tabla6PeliculasFiltradasCDB;
    private javax.swing.JTable tabla7PeliculasFiltradasCDB;
    private javax.swing.JTable tabla8CostoDeLosBoletosDF;
    private javax.swing.JTable tabla9CostoDeLosBoletosNBC;
    private javax.swing.JTextField txfCedulaDelClienteF;
    private javax.swing.JTextField txfClasificacionDePeliculaRP;
    private javax.swing.JTextField txfCostoDeLaEntradaDCF;
    private javax.swing.JTextField txfDireccionDelClienteF;
    private javax.swing.JTextField txfNombreDeLaPeliculaRP;
    private javax.swing.JTextField txfNombreDelClienteF;
    private javax.swing.JTextField txfNumeroDeBoletosAdultoMayorConDiscapacidadNBC;
    private javax.swing.JTextField txfNumeroDeBoletosDeAdolescenteConDiscapacidadNBC;
    private javax.swing.JTextField txfNumeroDeBoletosDeAdolescenteNBC;
    private javax.swing.JTextField txfNumeroDeBoletosDeAdultoConDiscapacidadNBC;
    private javax.swing.JTextField txfNumeroDeBoletosDeAdultoMayorNBC;
    private javax.swing.JTextField txfNumeroDeBoletosDeAdultoNBC;
    private javax.swing.JTextField txfNumeroDeBoletosDeNiñoConDiscapacidadNBC;
    private javax.swing.JTextField txfNumeroDeBoletosDeNiñoNBC;
    private javax.swing.JTextField txfNumeroDeTarjetaF;
    private javax.swing.JTextField txfRutaDeLaPortadaRP;
    private javax.swing.JTextField txfSinopsisDeLaPeliculaRP;
    private javax.swing.JTextField txfTelefonoDelClienteF;
    // End of variables declaration//GEN-END:variables
}
